---
title: Forgejo monthly update - October 2024
publishDate: 2024-10-31
tags: ['news', 'report']
excerpt: Forgejo is two years old and has been a lively human adventure, a story worth telling. A hackathon organized by Codeberg generated thousands of new translations. Forgejo v9.0.0 was published, as well as a security patch release which was backported to Forgejo v7, the six month old Long Term Support version. A kubernetes cluster was created to replace the current infrastructure, running Forgejo from the Helm Chart.
---

The monthly report is meant to provide a good overview of what has changed in Forgejo in the past month. If you would like to help, please get in touch in [the chatroom](https://matrix.to/#/#forgejo-chat:matrix.org) or participate in the [ongoing discussions](https://codeberg.org/forgejo/discussions).

## Two years - a recap

In October 2022 [Forgejo was announced](https://gitea-open-letter.coding.social/) in reaction to the takeover of [Gitea](https://en.wikipedia.org/wiki/Gitea#Forgejo_fork). After a two months of preparation, [the first release](https://forgejo.org/2022-12-26-monthly-update/) was published and [Codeberg announced](https://blog.codeberg.org/codeberg-launches-forgejo.html) using it because _"it provides Codeberg with an essential feature: trust"_.

The [security team](https://forgejo.org/2023-01-31-monthly-update/#security-releases) got quite busy soon and [published multiple releases](https://forgejo.org/2023-01-31-monthly-update/). The release team was also able to deliver but [a mistake was made](https://forgejo.org/2023-02-12-tags/). This was the first occasion for Forgejo to show that problems are explained transparently and their impact articulated clearly. The integrated CI, Forgejo Actions, [was announced](https://forgejo.org/2023-02-27-forgejo-actions/) and [started to be used by Forgejo itself](https://forgejo.org/2023-03-monthly-update/#dogfooding-forgejo-actions) very early on.

In February 2023 someone new (who wasn’t a contributor that the project is relying on) joined the chat and issue tracker, spoke repeatedly in ways that was hurtful/painful to Forgejo community members, and did not seem to have capacity to speak more sensitively, despite offers for support and repeated requests. [It distracted community members from productive and important work](https://forgejo.org/2023-03-monthly-update/#the-forgejo-community-is-healing) on governance, strategy and development. Some community members went silent, others were on edge. The [moderation process](https://codeberg.org/forgejo/governance/src/commit/5c07b3801537212ed6be1edfec298d7b004ce92d/MODERATION-PROCESS.md) was created during these challenging times. It took months for the community to heal.

In search for long term sustainability, [the first grant application](https://codeberg.org/forgejo/sustainability/issues/1) was sent. It was awarded and the funds allowed Codeberg to hire developers early 2024. It was not perfectly managed and [in December 2024](https://forgejo.org/2024-09-monthly-update/#sustainability) a significant part of the funds will be returned because they were not spent. It currently is the priority of the sustainability team [established in August 2024](https://forgejo.org/2024-08-monthly-update/#sustainability).

After weeks of discussions, a decision was made to [welcome copyleft contributions in Forgejo](https://forgejo.org/2023-06-copyleft/) in June 2023. The [Forgejo decision making process](https://codeberg.org/forgejo/governance/src/commit/5c07b3801537212ed6be1edfec298d7b004ce92d/DECISION-MAKING.md) requires that all concerns are heard and answered before a decision is final. It takes long but is also a key to being inclusive. It became a reality [a year later, in August 2024](https://forgejo.org/2024-08-gpl/).

Forgejo federation is and will always be the highest priority of the Forgejo project. Every month, since the beginning, [updates on its progress](https://forgejo.org/2023-06-monthly-update/#state-of-the-forge-federation-2023-edition) are published. It is still not in a usable state, two years later, and that has caused some frustration [but the work continues](https://forgejo.org/2024-08-monthly-update/#federation).

In August 2023 [a regression was discovered](https://forgejo.org/2023-08-release-v1-20-3-0/#fixing-the-risk-of-data-loss-related-to-storage-sections) to cause data loss. A lot of work went into fixing it and publishing documentation explaining how to recover. It was caused by a refactor that was not properly tested and was one of the main motivation to require that [every pull request merged in Forgejo is tested](https://codeberg.org/forgejo/governance/src/commit/5c07b3801537212ed6be1edfec298d7b004ce92d/PullRequestsAgreement.md).

In [the](https://forgejo.org/2023-09-monthly-update/) [last](https://forgejo.org/2023-10-monthly-update/) [months](https://forgejo.org/2023-11-monthly-update/) of 2023, Forgejo contributors kept improving while rebasing all the changes on top of the Gitea codebase. However, when Gitea Cloud was announced in December 2023 and after [some investigation](https://codeberg.org/forgejo/discussions/issues/92), it became clear that [Gitea turned Open Core](https://codeberg.org/forgejo/discussions/issues/102).

In January 2024, [the Forgejo localization](https://forgejo.org/2024-01-monthly-update/#localization) team came into existence, in anticipation of a hard fork. Before that, the Forgejo translations depended on Gitea translations which are trapped in a proprietary service. The initial localization team covered Arabic, Dutch, French, Russian, Greek and German and kept growing since.

Forgejo was ready for such an event and [declared its intention to become a hard fork](https://forgejo.org/2024-02-forking-forward/), separating itself from Gitea even further. Just as for the decision to welcome copyleft contributions, this required weeks of (sometime intense) discussions. And it also [took weeks of work to be implemented](https://forgejo.org/2024-02-monthly-update/#implementation-of-the-hard-fork) in March 2023. Coincidentally the Open Core turn of Gitea was confirmed when a the first proprietary version of Gitea was announced around the same time.

There was a sense of liberation when the hard fork began: it was possible to write code incompatible with the Gitea codebase! But there was also a price to pay: features and bug fixes relying on such code could not be shared with Gitea. It would have been easy to be carried away and get stuck with not enough contributors to maintain a codebase that diverged too quickly. To mitigate that risk [dependency management tooling](https://forgejo.org/2024-04-monthly-update/#dependency-management) and a weekly observation of Gitea activity was organized and is still in place.

The Forgejo v9.0 release that [was published in October 2024](https://forgejo.org/2024-10-release-v9-0/) is the third major release after the hard fork. It includes a feature that would have never been possible before (quotas) because it requires architectural changes conflicting with the Gitea codebase. Forgejo v7 is a [Long Term Support release](https://forgejo.org/2024-04-release-v7-0/), the first of its kind, supported during a year instead of three months. It is another benefit of the hard fork, made possible because Forgejo is no longer bound to the Gitea release cycle.

In these past two years Forgejo matured and transformed into an independent project, with a solid user base and a lively community of contributors. It involved a lot of coding and other time consuming technical work. But it was first and foremost a human adventure, with its share of plot twists and drama.

## Forgejo releases

On 16 October [Forgejo v9.0 was published](https://forgejo.org/2024-10-release-v9-0/). It is the first version to be released under a copyleft license. Codeberg was upgraded a week later. Regressions were discovered and fixed. Some of them were only noticeable visually (diagrams not showing labels or the displayed name of archives). Another was about the container image size that grew significantly ([180MB for v9.0.0](https://code.forgejo.org/forgejo/-/packages/container/forgejo/9.0.0)) and was reduced to [70MB for v9.0.1](https://code.forgejo.org/forgejo/-/packages/container/forgejo/9.0.1), back to the size of the [Forgejo v7](https://code.forgejo.org/forgejo/-/packages/container/forgejo/7) images.

On 28 October [Forgejo v9.0.1](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#9-0-1) was published and fixes those regressions. It also contains two security fixes that were backported and published as [Forgejo v7.0.10](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-10), the Long Term Support version.

These releases are the first to reflect the new Forgejo lifecycle. Before Long Term Release support existed, only v9 and v8 would have been supported, i.e. the last two major versions. But since [v7 is supported until July 2025](https://forgejo.org/docs/next/contributor/release/#release-cycle), the supported versions are now v9 and v7, i.e. the latest version and the long term support version.

## User Research

The user research team conducted [a survey regarding the repository settings](https://codeberg.org/forgejo/user-research/src/branch/main/surveys/repository-settings) during two weeks in October. It encouraged participation by showing banners to users of Codeberg in the repository section, linking to an external survey on Cryptpad.

There have been 118 submissions and the analysis is still ongoing, but there is already valuable feedback among the reviewed feedback. Thanks to all the participants!

There has been a rather [spontaneous interview regarding accessibility](https://codeberg.org/forgejo/user-research/src/commit/cd211e8cd40497a5e6e677a9d38b5450a2f519f9/interviews/other-feedback/2024-10-10-accessibility.md) with a Codeberg user. They reported a serious issue with their screen reader, which we didn't yet manage to reproduce (even after a contributor set up a test environment with the proprietary operating system and the screen reader). Investigation of this issue currently has high priority and we hope to fix the issues as soon as possible.

## Security Policy

Forgejo [published](https://codeberg.org/forgejo/governance/pulls/185) its [security policy](https://codeberg.org/forgejo/governance/src/branch/main/SECURITY-POLICY.md) to clarify communication and collaboration of the Forgejo security team with external parties such as libraries, security researchers and users.

Advance notice of security releases are [available publicly](https://codeberg.org/forgejo/security-announcements/issues). They do not contain specific information until the day of the release and are meant to help Forgejo admin plan for an upgrade.

Gitea was given a detailed description of the [security issues](https://codeberg.org/forgejo/forgejo/milestone/8544) fixed in the the v9.0.1 and v7.0.10 releases in advance, as well as a patch waiving copyright to fix them. From now on, any third party willing to receive such details in advance is required to explicitly agree to comply with the security policy.

## Helm chart

A new major version, [10.0.0](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v10.0.0) was published. It updates the Forgejo docker tag to v9.

The Forgejo helm chart had [security patch updates](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases), in both v7 and v10. [Helm chart v7.1.3](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v7.1.3) and [v10.0.1](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v10.0.1) are the latest.

## Localization

The [translation hackathon (translathon)](https://codeberg.org/Codeberg/translathon-2024#translathon-2024) organized by Codeberg this month resulted in many new contributors joining and making thousands of additions and improvements.

In total, 57 people contributed to the translations this month, which is significantly more than any previous month.

<!--
A new language was added and fully translated: Low German. It will take to proofread and polish it up before making it widely available to all users. Codeberg is planning to make it available for testing soon and it is planned to make it generally available with Forgejo v10.
Not sure if it's worth mentioning. The language isn't popular and announcing it will put pressure on a single person.
-->

A [new script](https://codeberg.org/forgejo/forgejo/pulls/5703) was added to process the localization files and verify that they contain only valid HTML insertions that follow the strictly defined rules. This should make it nearly impossible to insert malicious HTML.

Due to project's legacy, the localization strings traditionally were able to contain any arbitrary HTML code and often had hardcoded links and other aging code. The addition of this script reduces the number of attack vectors on Forgejo's codebase and improves it's maintainability. Fortunately, there have been no security incidents caused by this flaw.

## Infrastructure

A new [k8s cluster](https://code.forgejo.org/infrastructure/k8s-cluster/src/commit/42b69d45dc19bfcca53b7174c4b394c89bb3d8c6/README.md) was created and planned to replace the [current setup](https://code.forgejo.org/infrastructure/documentation/src/commit/31044c95882a4dd9b3c463c81f060586f2dc96f2/README.md). Instead of ad-hoc scripts, conventions and associated documentation, it relies on a [declarative description](https://code.forgejo.org/infrastructure/k8s-cluster/src/commit/42b69d45dc19bfcca53b7174c4b394c89bb3d8c6/flux) that updates the cluster when a commit is pushed to the repository.

It went through a few disaster recovery tests and is now in production, hosting https://next.forgejo.org and https://v7.next.forgejo.org, ready to welcome other Forgejo instances.

The motivation for creating this new cluster is to improve the availability of https://code.forgejo.org in the [wake of last month downtime](https://forgejo.org/2024-09-monthly-update/#infrastructure). But it also significantly improves automation and reduces the technical debt. It will obsolete the ad-hoc scripts ([wakeup-on-logs](https://code.forgejo.org/infrastructure/wakeup-on-logs), [shell scripts](https://code.forgejo.org/infrastructure/documentation/src/commit/31044c95882a4dd9b3c463c81f060586f2dc96f2/README.md), ...), conventions and [documentation](https://code.forgejo.org/infrastructure/documentation/src/commit/31044c95882a4dd9b3c463c81f060586f2dc96f2/README.md).

A k8s cluster is more attractive to Forgejo contributors who are willing to improve and maintain the infrastructure. They are in familiar territory if they already know k8s and do not need to learn new tools. They can start contributing with pull requests to the [repository describing the cluster](https://code.forgejo.org/infrastructure/k8s-cluster) and eventually apply to become a member of the devops team when they gained enough trust.

It is a lot more work to learn k8s from scratch than it is to learn the current ad-hoc system from scratch. From that point of view, this transformation does not make it easier to find volunteers willing to participate. However, there are a lot of devops who already learned k8s while nobody knows the current ad-hoc system. They do not need to learn k8s and can jump right in.

## Sustainability

The beneficiaries of the NLnet grant application sent in April 2024 are no longer available. A [call for participation](https://codeberg.org/forgejo/sustainability/issues/63#issuecomment-2391355) was posted to find Forgejo contributors willing to participate.

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/0ko
- https://codeberg.org/242336
- https://codeberg.org/413x1nkp
- https://codeberg.org/aleksi
- https://codeberg.org/algernon
- https://codeberg.org/AliveDevil
- https://codeberg.org/amano.kenji
- https://codeberg.org/anbraten
- https://codeberg.org/artnay
- https://codeberg.org/atarwn
- https://codeberg.org/Atul_Eterno
- https://codeberg.org/avobs
- https://codeberg.org/balinteus
- https://codeberg.org/be4zad
- https://codeberg.org/behm
- https://codeberg.org/Benny
- https://codeberg.org/Beowulf
- https://codeberg.org/brainiac
- https://codeberg.org/buhtz
- https://codeberg.org/caesar
- https://codeberg.org/CDN18
- https://codeberg.org/cdotnow
- https://codeberg.org/chrisnicola
- https://codeberg.org/ChrSt
- https://codeberg.org/cider
- https://codeberg.org/CL0Pinette
- https://codeberg.org/Crown0815
- https://codeberg.org/cryptolukas
- https://codeberg.org/Cyborus
- https://codeberg.org/d-s
- https://codeberg.org/DamianT
- https://codeberg.org/danshearer
- https://codeberg.org/David-Guillot
- https://codeberg.org/dawn-solace
- https://codeberg.org/ddogfoodd
- https://codeberg.org/Dirk
- https://codeberg.org/div72
- https://codeberg.org/dmowitz
- https://codeberg.org/dragon
- https://codeberg.org/earl-warren
- https://codeberg.org/edgalligan
- https://codeberg.org/eldyj
- https://codeberg.org/Ellpeck
- https://codeberg.org/emansije
- https://codeberg.org/Ember
- https://codeberg.org/etescartz
- https://codeberg.org/ewfg
- https://codeberg.org/ezra
- https://codeberg.org/feroli
- https://codeberg.org/Fjuro
- https://codeberg.org/floss4good
- https://codeberg.org/fnetX
- https://codeberg.org/Fnurkla
- https://codeberg.org/foxy
- https://codeberg.org/fuggla
- https://codeberg.org/Gateway31
- https://codeberg.org/GDWR
- https://codeberg.org/gregdechene
- https://codeberg.org/grgi
- https://codeberg.org/grosmanal
- https://codeberg.org/Gusted
- https://codeberg.org/hankskyjames777
- https://codeberg.org/herzenschein
- https://codeberg.org/io7m
- https://codeberg.org/iustin
- https://codeberg.org/jaahas
- https://codeberg.org/jacobwillden
- https://codeberg.org/JakobDev
- https://codeberg.org/jalil
- https://codeberg.org/jean-daricade
- https://codeberg.org/jerger
- https://codeberg.org/jogibear9988
- https://codeberg.org/JoseDouglas26
- https://codeberg.org/julianmarcos
- https://codeberg.org/jutty
- https://codeberg.org/jwakely
- https://codeberg.org/KaKi87
- https://codeberg.org/kecrily
- https://codeberg.org/kidsan
- https://codeberg.org/Kidswiss
- https://codeberg.org/kita
- https://codeberg.org/kmpm
- https://codeberg.org/kuolemaa
- https://codeberg.org/Kwonunn
- https://codeberg.org/kwoot
- https://codeberg.org/kytta
- https://codeberg.org/l_austenfeld
- https://codeberg.org/lapo
- https://codeberg.org/Laxystem
- https://codeberg.org/lime360
- https://codeberg.org/lingling
- https://codeberg.org/Link1J
- https://codeberg.org/lippoliv
- https://codeberg.org/lumi200
- https://codeberg.org/LunarLambda
- https://codeberg.org/lynoure
- https://codeberg.org/MaddinM
- https://codeberg.org/mahlzahn
- https://codeberg.org/Mai-Lapyst
- https://codeberg.org/malik
- https://codeberg.org/marcellmars
- https://codeberg.org/marcoaraujojunior
- https://codeberg.org/marshmallow
- https://codeberg.org/martinwguy
- https://codeberg.org/matrss
- https://codeberg.org/Merith-TK
- https://codeberg.org/meskobalazs
- https://codeberg.org/michael-sparrow
- https://codeberg.org/mikolaj
- https://codeberg.org/minecraftchest1
- https://codeberg.org/mzhang
- https://codeberg.org/n0toose
- https://codeberg.org/NameLessGO
- https://codeberg.org/natct
- https://codeberg.org/neilvandyke
- https://codeberg.org/neonew
- https://codeberg.org/nette
- https://codeberg.org/nick3331
- https://codeberg.org/niklaskorz
- https://codeberg.org/Nordfriese
- https://codeberg.org/nostar
- https://codeberg.org/ntn888
- https://codeberg.org/ossie
- https://codeberg.org/Outbreak2096
- https://codeberg.org/overloop
- https://codeberg.org/pat-s
- https://codeberg.org/patdyn
- https://codeberg.org/patrickuhlmann
- https://codeberg.org/petris
- https://codeberg.org/pgmtx
- https://codeberg.org/pinskia
- https://codeberg.org/poVoq
- https://codeberg.org/q3yi
- https://codeberg.org/qwerty287
- https://codeberg.org/removewingman
- https://codeberg.org/reynir
- https://codeberg.org/rvba
- https://codeberg.org/sandebert
- https://codeberg.org/SebasRebazCoding
- https://codeberg.org/SerikaFrame
- https://codeberg.org/sinsky
- https://codeberg.org/SinTan1729
- https://codeberg.org/SmolLemon
- https://codeberg.org/snematoda
- https://codeberg.org/Snoweuph
- https://codeberg.org/SomeTr
- https://codeberg.org/SpareJoe
- https://codeberg.org/SR-G
- https://codeberg.org/stb
- https://codeberg.org/SteffoSpieler
- https://codeberg.org/stevenroose
- https://codeberg.org/strypey
- https://codeberg.org/thefinn93
- https://codeberg.org/theycallhermax
- https://codeberg.org/thodorisl
- https://codeberg.org/tilegg
- https://codeberg.org/timedin
- https://codeberg.org/tkbremnes
- https://codeberg.org/tmb
- https://codeberg.org/Tom3201
- https://codeberg.org/vadim
- https://codeberg.org/viceice
- https://codeberg.org/voltagex
- https://codeberg.org/wangito33
- https://codeberg.org/whitecold
- https://codeberg.org/William_Weber_Berrutti
- https://codeberg.org/WithLithum
- https://codeberg.org/Wuzzy
- https://codeberg.org/xenrox
- https://codeberg.org/Xinayder
- https://codeberg.org/xtex
- https://codeberg.org/yeziruo
- https://codeberg.org/yoctozepto
- https://codeberg.org/yonas
- https://codeberg.org/ZzenlD

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
