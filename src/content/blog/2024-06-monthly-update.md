---
title: Forgejo monthly update - June 2024
publishDate: 2024-06-30
tags: ['news', 'report']
excerpt: The User Research effort that gained momentum two months ago continues with a new round of user testing sessions. It is key to collect evidence about what Forgejo users need, a requirement to build a roadmap and reduce the growing backlog of bug reports and feature requests. Building blocks for both ActivityPub federation and data portability improvements were merged into the codebase. They are not yet used for any user visible feature but they are a stepping stone. Their implementation was made significantly easier by the hard fork because they can rely on a codebase that is better tested.
---

The monthly report is meant to provide a good overview of what has changed in Forgejo in the past month. If you would like to help, please get in touch in [the chatroom](https://matrix.to/#/#forgejo-chat:matrix.org) or participate in the [ongoing discussions](https://codeberg.org/forgejo/discussions).

The [User Research effort](https://codeberg.org/forgejo/discussions/issues/156) that gained momentum two months ago continues with a new round of [user testing sessions](https://codeberg.org/forgejo/user-research/issues/34). It is key to collect evidence about what Forgejo users need, a requirement to build a roadmap and reduce the [growing backlog](https://codeberg.org/forgejo/forgejo/issues) of bug reports and feature requests.

Building blocks for both [ActivityPub federation](https://codeberg.org/forgejo/forgejo/pulls/1680) and [data portability improvements](https://codeberg.org/forgejo/forgejo/pulls/3590) were merged into the codebase. They are not yet used for any user visible feature but they are a stepping stone. Their implementation was made significantly easier by the hard fork because they can rely on a codebase that is better tested.

## User Research

Discussions began on [a new workflow for design work and feature requests](https://codeberg.org/forgejo/discussions/issues/178). Users are encouraged to describe the problem rather than the solution. Feature requests typically consist of the description of a solution. However, often there are multiple solutions to one problem, and there are multiple problems with one solution.

New feature requests are not ready to implement: they are investigated first, similar to how new bugs are triaged. A research and design process can be established, similar to what was done for the [summary tab for repositories](https://codeberg.org/forgejo/forgejo/issues/4119) and bootstrap actual process for changes in Forgejo. This adds a step between what user needs and development, but since the backlog of feature requests is growing anyway, it may not be an issue. On the contrary: merging similar feature requests together can reduce the backlog of open feature requests and only the most valuable changes are implemented.

A new round of [user testing sessions](https://codeberg.org/forgejo/user-research/issues/34) was conducted from 26 June to 28 June. It is [based on an interview script](https://codeberg.org/forgejo/user-research/src/branch/main/interviews/2024-06/template.md) improved from the one that was already used [in April 2024](https://codeberg.org/forgejo/user-research/src/branch/main/interviews/2024-04/template.md) during the first round.

## Features

Notable improvements:

- [Better logic for showing user feed/public activity elements](https://codeberg.org/forgejo/forgejo/pulls/4189)
- [Folding results for repo search](https://codeberg.org/forgejo/forgejo/pulls/4134)
- [Implement tab indentation and list continuation in the issue editor](https://codeberg.org/forgejo/forgejo/pulls/4072)
- [Stars federated via ActivityPub](https://codeberg.org/forgejo/forgejo/pulls/1680)
- [Support redis alternative - redict and garnet](https://codeberg.org/forgejo/forgejo/pulls/4138)
- [F3 initial driver](https://codeberg.org/forgejo/forgejo/pulls/3590)

[Read more](https://codeberg.org/forgejo/forgejo/pulls?labels=209916) in the pull requests.

## Standard formats and protocols

Forgejo supports third party software and services that use standards. For instance it relies on SQL for interactions with databases, HTTP, HTML and JavaScript for web interactions. As an exception when the software or service is available under a Free Software license, the absence of standardized communication protocol is not a requirement, for instance for [LevelDB](https://github.com/google/leveldb).

It is not an easy goal to achieve because software publishers keep deviating from standards or change the license of their software to no longer be Free Software.

- Microsoft SQL Server deviates from the SQL standard in [ways that required special treatment](https://codeberg.org/forgejo/forgejo/commit/2d9afd0c2194b60689717c2a9dc36284f012f7b6#diff-c286fd6672a72eeb3c97959eb3af0bf43959334a) and the decision was made two months ago to no longer include such specific adjustments.
- Redis used to be Free Software, but the newer versions [are not](https://web.archive.org/web/20240511181042if_/https://redis.io/legal/licenses/). The protocol is not standard but alternative servers exist and [Forgejo is now tested to work with them](https://codeberg.org/forgejo/forgejo/pulls/4138). Only the older Free Software version of Redis will continue to be supported, but the proprietary versions will not.
- Safari is expected to support HTML, HTTP & JavaScript in the same way other browsers do, but fails some tests. The approach chosen was to [skip Safari testing](https://codeberg.org/forgejo/forgejo/pulls/3334/files#diff-d98036e3541a0bc9dcffae7c5b40a8bfeb670760) instead of finding a workaround. An effort is generally made to workaround the non-conformant behavior of Free Software Web browsers. But, for reasons similar to MSSQL, such an effort is not sustainable for proprietary Web browsers.

Other discussions happened on integrating Forgejo with third party software or services that are not Free Software and do not comply with a standard format or protocol ([Azure Blob Storage](https://codeberg.org/forgejo/forgejo/pulls/3989), [CockroachDB](https://codeberg.org/forgejo/discussions/issues/174) and [Friendly Captcha](https://codeberg.org/forgejo/forgejo/pulls/4154)).

## Data portability

The [Friendly Forge Format (F3)](https://f3.forgefriends.org/) is designed to improve data portability and make it possible to mirror software projects from one forge to another using a standard format. The [v2.0.0](https://f3.forgefriends.org/compliance.html) was published in June 2024 and is the first stable version. It is implemented in [a Go package](https://code.forgejo.org/f3/gof3) that can be used as a reference. A native driver for Forgejo [was merged](https://codeberg.org/forgejo/forgejo/pulls/3590) so that it can be used, for instance, to transport data when an ActivityPub message is received. Or as an alternate implementation for the migration code.

Read more in F3 related [issues](https://codeberg.org/forgejo/forgejo/issues?labels=114735) and [pull requests](https://codeberg.org/forgejo/forgejo/pulls?labels=114735).

## Federation

The pull request to implement [federated repository stars](https://codeberg.org/forgejo/forgejo/pulls/1680) was merged. The feature allows you to define following repositories such as origin of mirrors or forks. If then the mirror or fork gets a star by someone, the origin repository will also get a star via federated ActivityPub like activity. Only the star activity is federated, the unstar activity is not federated and the user interface is not available yet.

A new federation suite was [added to the end-to-end tests](https://code.forgejo.org/forgejo/end-to-end/src/branch/main/federation). It is very basic but the first of its kind. Launching two Forgejo instances that [star each other via ActivityPub](https://code.forgejo.org/forgejo/end-to-end/src/branch/main/federation/scenario-star/run.sh).

Although GitLab federation efforts are [now on pause](https://codeberg.org/forgejo/discussions/issues/184) it includes an experimental implementation of the ActivityPub building blocks which is roughly at the same stage as Forgejo. Support for [launching a GitLab instance](https://code.forgejo.org/forgejo/end-to-end/src/commit/e4fa6d814fce398fd4065e40c67a53acbbf2e3a5/lib/lib.sh#L199-L227) was added to the end-to-end tests. This will eventually allow to play scenarios in which GitLab and Forgejo can communicate in a controlled environment. And in the short term to apply minimal testing on each of them independently, for instance with [activitypub-testing](https://codeberg.org/socialweb.coop/activitypub-testing).

Read more [in the June 2024 report](https://domaindrivenarchitecture.org/posts/2024-06-05-howto-federated-stars/).

## UI team

The [UI team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#user-interface) was [created](https://codeberg.org/forgejo/governance/pulls/128) and has [three members](https://codeberg.org/forgejo/governance/pulls/144).

Before engaging in more ambitious goals, they started to care for the day to day chores that keep Forgejo releases going:

- Implementing and reviewing [new UI features](https://codeberg.org/forgejo/forgejo/pulls?state=closed&labels=87703%2c78139&milestone=6042) which is particularly challenging because the test coverage of the Web UI is still very low.
- Backporting bug fixes to the stable versions ([7.0.4](https://codeberg.org/forgejo/forgejo/pulls?labels=87703&milestone=6405) & [7.0.5](https://codeberg.org/forgejo/forgejo/pulls?labels=87703&milestone=6654)).
- Reviewing [upgrades](https://codeberg.org/forgejo/forgejo/pulls?labels=223008) of [JavaScript packages](https://codeberg.org/forgejo/forgejo/src/commit/8afdafebf9fa2cb748a13e56ff3d865675ae27b6/package.json) and [other dependencies](https://codeberg.org/forgejo/forgejo/pulls?labels=85536) that are used to build the Web UI. It would be relatively easy if every package consistently complied with [semantic versioning](https://semver.org/) and published descriptive release notes. But the norm is rather the opposite and each potential upgrade requires a fair amount of scrutiny.

An experiment to hire a freelance to work on improving the JavaScript test coverage was conducted to figure out if it leads to results that are worth the effort. It was interrupted when evidence surfaced that the work was almost identical to what an AI would produce.

## Infrastructure

The [machine](https://forgejo.org/docs/next/developer/infrastructure/#hetzner0104) added last month is now in production and stable. It did not go smoothly because the hardware provisioned turned out to be unreliable and crashed frequently. The devops team spent hours over a period of a week to figure out the root cause and stabilize it. This was not transparent to Forgejo contributors because the CI stopped working half a dozen time and stayed down during a few hours. But nothing was lost and the downtime was kept to a minimum. It happened in between releases and did not disrupt the release process.

The upside of these troubles was to check that every aspect of the disaster recovery scenario work as it should, including loosing all disks on the machine. The defective hardware was not stabilized, it could not. It turned out that all machines with the same motherboard were similarly flawed. Hetzner acknowledged their defective product range, apologized, reimbursed the costs and is in the process of changing their Q&A to include the test script that was provided to demonstrate the problem.

## Releases

[Forgejo v7.0.4](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-4) (fixing [vulnerabilities](https://codeberg.org/forgejo/security-announcements/issues/11)) was released. The security fix it contains was also [backported and released as Forgejo v1.21.11-2](https://codeberg.org/forgejo/forgejo/releases/tag/v1.21.11-2).

There now is a [stronger requirement for testing](https://codeberg.org/forgejo/discussions/issues/168) for all backports to a stable release. The bug fixes originating from Gitea were previously merged even when they were not covered by any test. Tests are now added when the fix is worth the effort. The test itself is implemented [in the development branch](https://codeberg.org/forgejo/forgejo/pulls/4217) and the commit that contains the fix is [cherry-picked with the backport of the test](https://codeberg.org/forgejo/forgejo/pulls/4219) to demonstrate it works as it should.

A discussion regarding the [Forgejo v8.0.0 feature freeze](https://codeberg.org/forgejo/discussions/issues/180) an attempt to better define the requirements imposed during such a period and the [need to fix bugs when the infrastructure to write tests is missing](https://codeberg.org/forgejo/discussions/issues/180#issuecomment-2043685).

## Dependency Management

Caring for the hundreds of Forgejo dependencies has been a recurring activity since the beginning of the project back in 2022. When the hard fork happened early 2024 Forgejo gained the freedom to decide on each of them and began to organize accordingly. Gitea was first and using the [dedicated tool](https://codeberg.org/forgejo/tools/src/branch/main/scripts/wcp) that was developed to sort the commits of interest became a [comfortable weekly routine](https://codeberg.org/forgejo/forgejo/pulls?q=week+2024&state=closed).

The backlog of all other dependencies was more challenging. A [dependency dashboard was setup](https://codeberg.org/forgejo/forgejo/issues/2779) and started with a large backlog. Only in June 2024 was it cleared, months later. Pull requests with upgrades are [now dealt with daily](https://codeberg.org/forgejo/forgejo/issues?labels=223008) and diligently so they do not pile up again.

The method to analyze each of them [was documented](https://forgejo.org/docs/latest/developer/dependencies/) and heavily relies on the [dependency specifications](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/renovate.json) to keep the workload to a minimum. For instance it can arrange for updates to only be proposed every three months for CI tooling while it immediately proposes an upgrade if has an impact on security.

## Helm chart

The Forgejo helm chart had [two patch updates](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases), one for [7.0.1](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v7.0.1) which depends on Forgejo v7.0.4 and another for [5.1.2](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v5.1.2) which depends on Forgejo v1.21.11-2.

The [Forgejo helm team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#helm) was [created](https://codeberg.org/forgejo/governance/pulls/136) was created and [two members joined](https://codeberg.org/forgejo/governance/pulls/149).

## Localization

4 batches of updates were merged containing total of 937 new translations and 518 improvements. Some improvements were made to the process of localization management. One of which is that now backporting of translation updates to stable versions can happen in batches before releases are published instead of weekly.

No new team applications were submitted or accepted this month. Some languages are not actively maintained currently. View completeness of translations in our project on [Codeberg Translate](https://translate.codeberg.org/projects/forgejo/forgejo) and [Learn how to help](https://forgejo.org/docs/latest/developer/localization/).

## Sustainability

There is a [new funding opportunity](https://codeberg.org/forgejo/discussions/issues/185) for [Sovereign Tech Fund](https://www.sovereigntechfund.de/) and a [grant application is being drafted](https://codeberg.org/avobs/sovereign_tech/src/branch/main/application_text.md). Its scope is similar to [OTF](https://www.opentech.fund/funds/free-and-open-source-software-sustainability-fund/) and most of the material can be re-used.

The [grant proposal](https://codeberg.org/forgejo/sustainability/pulls?state=all&labels=217359) submitted 16 May for the [Free and Open Source Software Sustainability Fund](https://www.opentech.fund/funds/free-and-open-source-software-sustainability-fund/) was [declined](https://codeberg.org/forgejo/sustainability/pulls/48). In a newsletter OTF announced that they received 80 applications.

A reply was expected for the NLnet grant application [submitted 1 April](https://codeberg.org/forgejo/sustainability/pulls?labels=220838) but was further delayed and is expected in July.

## Moderation

There has been very few minor moderation incident (spam or trolls) and one time warning. This is significantly less than the past month where there were bursts of unsolicited activity that sometime required daily intervention. Even the smallest interventions were logged in accordance to the [moderation process](https://codeberg.org/forgejo/governance/src/branch/main/MODERATION-PROCESS.md).

A concern was raised regarding the [lack of answer of the floss.social moderation team](https://codeberg.org/forgejo/discussions/issues/87) despite numerous attempts over a period of months. There has been no major incident so far but it may be wise to take steps for Forgejo to be in a space where it is possible to reach the moderation team before it happens.

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/0ko
- https://codeberg.org/abacabadabacaba
- https://codeberg.org/Aeris1One
- https://codeberg.org/aimuz
- https://codeberg.org/algernon
- https://codeberg.org/Application-Maker
- https://codeberg.org/Atalanttore
- https://codeberg.org/atimy
- https://codeberg.org/avidseeker
- https://codeberg.org/avobs
- https://codeberg.org/b1nar10
- https://codeberg.org/bart
- https://codeberg.org/bbjubjub2494
- https://codeberg.org/bdube_gh
- https://codeberg.org/becm
- https://codeberg.org/bengrue
- https://codeberg.org/Beowulf
- https://codeberg.org/buhtz
- https://codeberg.org/crystal
- https://codeberg.org/cuboci
- https://codeberg.org/Cyborus
- https://codeberg.org/Cysioland
- https://codeberg.org/delvh
- https://codeberg.org/dev_T
- https://codeberg.org/Dirk
- https://codeberg.org/drawingpixels
- https://codeberg.org/earl-warren
- https://codeberg.org/efertone
- https://codeberg.org/el0n
- https://codeberg.org/emansije
- https://codeberg.org/f00
- https://codeberg.org/Fjuro
- https://codeberg.org/fnetX
- https://codeberg.org/fr33domlover
- https://codeberg.org/fractalf
- https://codeberg.org/gerald
- https://codeberg.org/gitcookie-1
- https://codeberg.org/GKuhn
- https://codeberg.org/GooRoo
- https://codeberg.org/GT
- https://codeberg.org/Gusted
- https://codeberg.org/h759bkyo4
- https://codeberg.org/hackos
- https://codeberg.org/Hanker
- https://codeberg.org/hankskyjames777
- https://codeberg.org/Haui
- https://codeberg.org/hazy
- https://codeberg.org/hoijui
- https://codeberg.org/how
- https://codeberg.org/ikuyo
- https://codeberg.org/jadeprime
- https://codeberg.org/jakjakob
- https://codeberg.org/jean-daricade
- https://codeberg.org/jerger
- https://codeberg.org/jthvai
- https://codeberg.org/jwildeboer
- https://codeberg.org/KaKi87
- https://codeberg.org/karolyi
- https://codeberg.org/kdh8219
- https://codeberg.org/kita
- https://codeberg.org/Kitanit
- https://codeberg.org/Kwonunn
- https://codeberg.org/l_austenfeld
- https://codeberg.org/leana8959
- https://codeberg.org/ledyba
- https://codeberg.org/lingling
- https://codeberg.org/Linneris
- https://codeberg.org/lordwektabyte
- https://codeberg.org/lotigara
- https://codeberg.org/Mai-Lapyst
- https://codeberg.org/martianh
- https://codeberg.org/martinwguy
- https://codeberg.org/mirkoperillo
- https://codeberg.org/mnq
- https://codeberg.org/mondstern
- https://codeberg.org/mritunjayr
- https://codeberg.org/mrwsl
- https://codeberg.org/n0toose
- https://codeberg.org/neomaitre
- https://codeberg.org/nilesh
- https://codeberg.org/oelmekki
- https://codeberg.org/oliverpool
- https://codeberg.org/overloop
- https://codeberg.org/patdyn
- https://codeberg.org/Pi-Cla
- https://codeberg.org/pmb
- https://codeberg.org/podhorsky-ksj
- https://codeberg.org/poVoq
- https://codeberg.org/programmerjake
- https://codeberg.org/proton-ab
- https://codeberg.org/purkwiat
- https://codeberg.org/qaqland
- https://codeberg.org/qwerty287
- https://codeberg.org/realaravinth
- https://codeberg.org/Renich
- https://codeberg.org/samcday
- https://codeberg.org/scruel
- https://codeberg.org/SDKAAA
- https://codeberg.org/Senku
- https://codeberg.org/sevki
- https://codeberg.org/silverpill
- https://codeberg.org/sinsky
- https://codeberg.org/SinTan1729
- https://codeberg.org/sirhectorin
- https://codeberg.org/snematoda
- https://codeberg.org/solemden
- https://codeberg.org/soundstrip
- https://codeberg.org/stdedos
- https://codeberg.org/sunwoo1524
- https://codeberg.org/tampler
- https://codeberg.org/tgy
- https://codeberg.org/thedustinmiller
- https://codeberg.org/thefox
- https://codeberg.org/Thesola10
- https://codeberg.org/ThetaDev
- https://codeberg.org/thetredev
- https://codeberg.org/toolforger
- https://codeberg.org/twenty-panda
- https://codeberg.org/uku
- https://codeberg.org/vai
- https://codeberg.org/viceice
- https://codeberg.org/vikaschoudhary
- https://codeberg.org/virtulis
- https://codeberg.org/vwbusguy
- https://codeberg.org/wintryexit
- https://codeberg.org/Wuzzy
- https://codeberg.org/XaviCC
- https://codeberg.org/Xinayder
- https://codeberg.org/xlii
- https://codeberg.org/yeziruo
- https://codeberg.org/yongbin

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
