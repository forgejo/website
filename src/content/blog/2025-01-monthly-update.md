---
title: Forgejo monthly update - January 2025
publishDate: 2025-02-11
tags: ['news', 'report']
excerpt: New releases, improvements to the website and an update to the current state of federation in Forgejo. January was a productive month that ended in preparations to FOSDEM and allowed some team members to meet in real life for the first time.
---

The monthly report is meant to provide a good overview of what has changed in Forgejo in the past month. If you would like to help, please get in touch in [the chatroom](https://matrix.to/#/#forgejo-chat:matrix.org) or participate in the [ongoing discussions](https://codeberg.org/forgejo/discussions).

## Releases

[Forgejo v10.0.0 was published](https://forgejo.org/2025-01-release-v10-0/) on 16 January, one day after the planned release date because [a regression](https://codeberg.org/forgejo/forgejo/issues/6575) was discovered at the last minute. Another issue surfaced a few days later, when migrating Forgejo instances that contained [orphaned records in a database table](https://codeberg.org/forgejo/forgejo/issues/6637). Such records are not found in any test environment and do not prevent Forgejo from running, but they interfered with the upgrade of two instances. Fortunately, there is a rather simple workaround and the [doctor is now able to dispose of them](https://codeberg.org/forgejo/forgejo/pulls/6639). Codeberg was able to upgrade without incident a week later.

The Forgejo release manager was [improved on this occasion](https://code.forgejo.org/forgejo/release-manager/commits/commit/8b19f663f41429f4c2e0f06a168a027670bb29b6). The tasks that are common between patch releases and major releases ([release-tasks](https://code.forgejo.org/forgejo/release-tasks), [release-announce](https://code.forgejo.org/forgejo/release-announce), [etc...](https://code.forgejo.org/forgejo/release-manager/src/commit/8b19f663f41429f4c2e0f06a168a027670bb29b6/.forgejo/workflows)) were re-used as-is. There are still a number of items that need to be done manually and they can gradually be automated within this framework.

The [Forgejo runner v6](https://code.forgejo.org/forgejo/runner/releases/tag/v6.2.1) major release was published continuing the effort to fix security issues and to bring it closer to a beta stage. It also contains [support for ephemeral runners](https://code.forgejo.org/forgejo/runner/pulls/423) and [force rebuilds](https://code.forgejo.org/forgejo/runner/pulls/406).

A [new major release](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases) of the Forgejo helm chart was published 15 January. It contained [a regression](https://code.forgejo.org/forgejo-helm/forgejo-helm/pulls/1056) that was fixed the next day, with [11.0.1](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v11.0.1). It has [a few breaking changes](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v11.0.0) and defaults to using Forgejo v10.0.0. The [Forgejo helm v7.1.7](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v7.1.7) patch release to be used with Forgejo v7 was also published.

## Translations

A PR initiating the [switch to a more flexible translation format](https://codeberg.org/forgejo/forgejo/pulls/6203) has been merged, however, a bug was discovered in Weblate that prevented a new component from being used right away, seemingly because Forgejo is pioneering usage of go-i18-v2 format in Weblate. @Gusted figured it out and [submitted a fix to upstream](https://github.com/translate/translate/pull/5471). It has been merged, but we're waiting for a Weblate update which will include that fix.

Filipino translations were completed (thanks to the effort of @kita) in December 2024 and have been released for the first time in the v10 release in January. Similarly, Lower German is now available as a new language. Especially since there are comparatively few speakers, we are proud to be among the software projects available in these locales.

## FOSDEM

Forgejo and Codeberg had a stand during [FOSDEM](https://fosdem.org) February 1 and 2 in Brussels. We might go into details about this in the February update (because, well, apart from travelling there most of it happened in February anyway and we still need to wrap things up on our end). You can read and participate in our [discussion about this year's FOSDEM](https://codeberg.org/forgejo/discussions/issues/290), feel free to share your impressions.

It was a pleasure talking to so many of you, and the first time many of our team members have seen each other in person. Thank you for your trust and support, and for distributing our stickers!

## Federation

If you follow the Forgejo monthly updates, you might have noticed that we haven't written about federation recently, and some of you asked about it. We apologize for the omission, but rest assured: Federation is still a high priority and we haven't given up on it.

Due to personal constraints, things have slowed down a little bit by the end of the year. With federated star, a first user-visible feature has been implemented and some follow-up PRs have been in development, but not yet merged. Federated unstar (which complements the starring of repos) will be the next task, but due to it being a little destructive, more backend work is happening. The developers briefly talked about this [during their FOSDEM talk](https://fosdem.org/2025/schedule/event/fosdem-2025-5610-show-and-tell-federation-at-forgejo/) and first code was [submitted by the end of January](https://codeberg.org/forgejo/forgejo/pulls/6740).

A [federation roadmap was developed](https://codeberg.org/forgejo-contrib/federation/src/branch/main/FederationRoadmap.md) and discussed, which provides the next steps and their requirements to make federation useful in some capacity. There is commitment to complete some of these milestones in 2025.

To ensure the federation project can move on regardless of limited funding capabilities, [it was agreed to compensate the current federation maintainers for maintenance jobs](https://codeberg.org/forgejo/governance/issues/215) such as code review and onboarding with up to € 500 / month. This ensures that community contributions to federation can be reviewed and merged quickly. The funding was [provided to Forgejo by Codeberg e.V.](https://codeberg.org/forgejo/sustainability/issues/72).

## Website and documentation

Forgejo actions were implemented to check for spelling mistakes upon pull requests, both for [documentation](https://codeberg.org/forgejo/docs/src/commit/42520fbfa70ab020817d7b91e0711890109e301a/.forgejo/workflows/pr.yml#L22) and [website](https://codeberg.org/forgejo/website/src/commit/545e218907e509910b535884d059e49949e4e533/.forgejo/workflows/pr.yml#L23) using the source code checker [typos](https://github.com/crate-ci/typos). To our positive surprise, there weren't many spelling mistakes on the website, which means that Forgejo's contributors already did a good job in detecting those during blog post draft approval.

Broken links are an annoyance to readers of the website. The amount of those in the documentation was [significantly reduced](https://codeberg.org/forgejo/docs/issues/583) thanks to the scheduled link check action. It uses the [lychee](https://lychee.cli.rs/) binary and was [added to the website pages](https://codeberg.org/forgejo/website/src/commit/545e218907e509910b535884d059e49949e4e533/.forgejo/workflows/links.yml), too. While implementing, too many requests were sent to [codeberg.org](https://codeberg.org) and mistaken for abuse, we're sorry for that! Fortunately, the server is set up with automatic mitigation of such events by blocking the concerned IP address for half an hour. The reason was the massive amount of links to [contributors](#we-forge), which are now [excluded from the daily link checks](https://codeberg.org/forgejo/website/src/commit/545e218907e509910b535884d059e49949e4e533/.lychee.toml#L9-L10).

Apart from the [usual documentation improvements](https://codeberg.org/forgejo/docs/activity/monthly), the documentation footer on the website has been improved to [properly attribute the authors of derived CC-licensed work](https://codeberg.org/forgejo/website/pulls/544) and the edit link has been replaced to point always to the related document in the [next](https://codeberg.org/forgejo/docs/src/branch/next) branch.

To further simplify the release process, when building the website, latest and LTS releases are now automatically retrieved using the recently introduced [release schedule](https://forgejo.org/2024-11-monthly-update/#forgejo-releases).

Work has started to improve the accessibility of the Forgejo's website, such as adding a global search and documentation version switches. Contributions are always welcome to both the [website source](https://codeberg.org/forgejo/website) and the [documentation](https://codeberg.org/forgejo/docs).

## Infrastructure

On the day of the Forgejo v10.0.0 release, the IP of the machine running the [workflows responsible for the release](https://codeberg.org/forgejo/forgejo/src/commit/5813244ff8283eb6f53fb124683a701636031433/.forgejo/workflows/build-release.yml) was rate limited by https://hub.docker.com/ which host most of the container images used in the CI. It disrupted a number of workflows and had to be dealt with immediately. All workflows were modified months ago to use [Forgejo hosted mirrors](https://code.forgejo.org/oci/-/packages) updated [daily](https://code.forgejo.org/forgejo/oci-mirror/src/commit/aa76bff0ec16a19096f45d9178e6a644b3e5faaa/.forgejo/workflows/mirror.yml) to prevent that kind of disruption but some references were overlooked. After they were fixed, the workflow responsible for the release was no longer rate limited and the release could proceed.

code.forgejo.org went down during 15 minutes on 21 January 2025. It was the first unscheduled downtime since [9 September](https://codeberg.org/forgejo/discussions/issues/220) which lasted about 10 hours. [Read more in the post-mortem](https://codeberg.org/forgejo/discussions/issues/267).

About two years ago, Forgejo action runners were installed manually on [a machine](https://code.forgejo.org/infrastructure/documentation#hetzner01) that is still in use today. It grew over time and around twenty were also [installed manually](https://code.forgejo.org/infrastructure/documentation/src/commit/b0cd0c924349fe01026db319c6d51a56032b222e/runner-lxc.md). Ideally they could all be migrated to the new k8s cluster but there is no LXC support in k8s and it is required for a number of workflows that cannot run out of application containers (podman or docker).

A [script managing a systemd unit dedicated to the runner](https://code.forgejo.org/forgejo/runner/src/commit/5df67e66fe2fb5ac7f3ef246d2158d457c6073c8/examples/lxc-systemd) was created and [used to manage all runners](https://code.forgejo.org/infrastructure/documentation/src/commit/3399353bd41396550ebcc2cea80eeea1bde6b8f2/runner-lxc.md) with familiar commands such as `systemctl start forgejo-runner@42`.

## Dependency management

The [forgejo dependency configuration](https://code.forgejo.org/forgejo/renovate-config/commits/commit/bf629bcb867def68e03f2c6e8e38ea4109f83a48/default.json) and the [renovate](https://code.forgejo.org/renovate) organization (formerly in `forgejo-contrib`) are the central point from which the dozens of repositories involved in the making of Forgejo (CI, tools or releases) are watched. It automates the obvious but tedious task of ensuring, for instance, that the go release found in `go.mod` has an up-to-date version with regard to security vulnerabilities.

Doing so for an individual repository (which was the starting point of that effort with [the main Forgejo repository](https://codeberg.org/forgejo/forgejo/src/commit/5813244ff8283eb6f53fb124683a701636031433/renovate.json) in 2024) is convenient but it does not scale well. To help with that, shared configurations (see the [one used by Forgejo](https://codeberg.org/forgejo/forgejo/src/commit/5813244ff8283eb6f53fb124683a701636031433/renovate.json#L3) for instance) have been created and underwent significant improvements this month.

In some cases, such as dependencies that are only used in the CI, it is OK for a pull request containing an upgrade to be merged unattended as soon as the workflows pass. This is what the automerge feature is for but it left a branch behind because it was not supported prior to Forgejo v10. The Forgejo driver for renovate [was patched](https://github.com/renovatebot/renovate/pull/33871/files#diff-ce0247a11f9ad18bc0beefe4553f73f3a50b146ca558f4afa9bae2dd2e7502ad) and there are no longer lingering branches after an automerge.

A number of additional repositories were added to [the pool of repositories](https://code.forgejo.org/renovate/renovate/src/commit/1a92eab6b45dc600f3ec42dd2a62e14828f461b3/src/config.json) that renovate watches and it is no longer necessary to individually approve them, as long as they belong to a known organization.

Because renovate was not in use in 2023 (or in some cases where scripting is involved), some dependencies are not specified in a way that renovate can natively discover. For those, dedicated rules are being developed which run the equivalent of `sed -i -e 's/old version/new version/'` on the designated file (e.g [upgrading the RUNNER_VERSION](https://code.forgejo.org/actions/setup-forgejo/src/commit/834d6ebe2a7f956c319721b2380a2b344c41af5b/renovate.json#L5-L14) file in `setup-forgejo`).

Now that dependency management has mature tooling in all Forgejo spaces, it is easier for both volunteers or paid staff to review each individual upgrade. There are just a few every day but most of them require human scrutiny. A [call for participation](https://codeberg.org/forgejo/discussions/issues/262) was posted and will hopefully be attractive to people willing to contribute to Forgejo.

## Moderation incidents

January was a little noisy regarding moderation actions. There have been cases of community members that created a lot of noisy issues, presumably with the help of AI tools. While probably in good faith, throwing walls of text by increasing the amount of open issues in forgejo/discussions by 10% in only a few days as well as responding with huge amounts of text to even simple questions was exceeding the capacity of our volunteers to deal with.

Additionally, some new contributors required a lot of help, started heated discussions, or obstinately pushed their own ideas even if they faced disagreement by several other community members. These incidents have created a slightly tense atmosphere for a few days.

Luckily, the community has openly talked about the incidents, both in issues and in the Matrix channel, and most of the problems could be resolved quickly. There have been cases where the noise has led to premature reactions by some maintainers, for example when a lengthy issue has been reported as "spam" or "trolling", even if they might have been created without bad intents.

We apologize if you have been impacted by any of these incidents, either because an issue you have reported has been flooded with heated discussions, or because a contribution from you has been flagged too quickly. If you think one of your contributions did not receive the attention it deserved, feel free to carefully reopen or recreate it, respecting the pace at which the community can possibly deal with them.

The exceptional level of tensions lasted for a few days and calmed down again. We expect that this was mainly bad timing to have multiple such cases at the same time and there are no signs this would repeat in the new future. Nevertheless, a new [moderator was proposed](https://codeberg.org/forgejo/governance/issues/219) to increase the human resources within the Forgejo moderation team.

Forgejo strives to be an inclusive project where everyone can participate in a safe environment. If you, as a community member, feel unsafe for any reason, feel free to reach out to the [moderation team](https://codeberg.org/forgejo/governance/src/branch/main/MODERATION-PROCESS.md#moderation-contact).

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/0ko
- https://codeberg.org/2franix
- https://codeberg.org/aimuz
- https://codeberg.org/Alex619829
- https://codeberg.org/algernon
- https://codeberg.org/amoutill
- https://codeberg.org/Andre601
- https://codeberg.org/angelnu
- https://codeberg.org/AntoninDelFabbro
- https://codeberg.org/Apfelwurm
- https://codeberg.org/aqtrans
- https://codeberg.org/artnay
- https://codeberg.org/ashimokawa
- https://codeberg.org/ashur_iraq
- https://codeberg.org/Atalanttore
- https://codeberg.org/Atul_Eterno
- https://codeberg.org/awiteb
- https://codeberg.org/bakaroni
- https://codeberg.org/banaanihillo
- https://codeberg.org/bartavi
- https://codeberg.org/bbastin
- https://codeberg.org/Be.ing
- https://codeberg.org/Beowulf
- https://codeberg.org/billynoah
- https://codeberg.org/bmorel
- https://codeberg.org/BtbN
- https://codeberg.org/burakozaydin
- https://codeberg.org/bvandevliet
- https://codeberg.org/c8h4
- https://codeberg.org/caesar
- https://codeberg.org/carnivorecookies
- https://codeberg.org/CEbbinghaus
- https://codeberg.org/Chealer
- https://codeberg.org/cloud
- https://codeberg.org/cobak78
- https://codeberg.org/ConfusedOnCOFFEE
- https://codeberg.org/Crown0815
- https://codeberg.org/crystal
- https://codeberg.org/cstaky
- https://codeberg.org/d96b
- https://codeberg.org/dallyger
- https://codeberg.org/daniel-baumann
- https://codeberg.org/DanielRuf
- https://codeberg.org/davbfr
- https://codeberg.org/davrot
- https://codeberg.org/Dirk
- https://codeberg.org/div72
- https://codeberg.org/doasu
- https://codeberg.org/Dreamboat9222
- https://codeberg.org/earl-warren
- https://codeberg.org/Edgarsons
- https://codeberg.org/elit-matas
- https://codeberg.org/ell1e
- https://codeberg.org/emansije
- https://codeberg.org/Ember
- https://codeberg.org/emilylange
- https://codeberg.org/endernon
- https://codeberg.org/fauno
- https://codeberg.org/filmroellchen
- https://codeberg.org/Fishwaldo
- https://codeberg.org/Fjuro
- https://codeberg.org/floss4good
- https://codeberg.org/flyingfishflash
- https://codeberg.org/fnetX
- https://codeberg.org/fossdd
- https://codeberg.org/foxy
- https://codeberg.org/Frankkkkk
- https://codeberg.org/Froini
- https://codeberg.org/fvezzoli
- https://codeberg.org/GKuhn
- https://codeberg.org/gratux
- https://codeberg.org/guinevere
- https://codeberg.org/Gusted
- https://codeberg.org/harbourcoleman
- https://codeberg.org/hashirama
- https://codeberg.org/hexa
- https://codeberg.org/Hiraku
- https://codeberg.org/hoppinglife
- https://codeberg.org/hucksy
- https://codeberg.org/ikidd
- https://codeberg.org/ikuyo
- https://codeberg.org/infinoid
- https://codeberg.org/Ironfractal
- https://codeberg.org/izzy
- https://codeberg.org/JadedBlueEyes
- https://codeberg.org/JakobDev
- https://codeberg.org/jasonb
- https://codeberg.org/javiertury
- https://codeberg.org/jedik
- https://codeberg.org/jerger
- https://codeberg.org/jkirk
- https://codeberg.org/jmakov
- https://codeberg.org/JohnMoon-VTS
- https://codeberg.org/jornfranke
- https://codeberg.org/JuniorJPDJ
- https://codeberg.org/justbispo
- https://codeberg.org/jutty
- https://codeberg.org/jwildeboer
- https://codeberg.org/KaKi87
- https://codeberg.org/kampka
- https://codeberg.org/kdh8219
- https://codeberg.org/kdtresh
- https://codeberg.org/khaeru
- https://codeberg.org/kidsan
- https://codeberg.org/killawabbit
- https://codeberg.org/kita
- https://codeberg.org/Kwonunn
- https://codeberg.org/lapo
- https://codeberg.org/Laxystem
- https://codeberg.org/lenikadali
- https://codeberg.org/lhinderberger
- https://codeberg.org/litchipi
- https://codeberg.org/lordwektabyte
- https://codeberg.org/lynnesbian
- https://codeberg.org/maarrk
- https://codeberg.org/mahlzahn
- https://codeberg.org/marcelovicentegc
- https://codeberg.org/MarcoZehe
- https://codeberg.org/MarkL4YG
- https://codeberg.org/marshmallow
- https://codeberg.org/Marti
- https://codeberg.org/martinwguy
- https://codeberg.org/mary-ext
- https://codeberg.org/matrss
- https://codeberg.org/mattie_sanz
- https://codeberg.org/mcnesium
- https://codeberg.org/mehrad
- https://codeberg.org/meisam
- https://codeberg.org/melonion
- https://codeberg.org/Merith-TK
- https://codeberg.org/mfenniak
- https://codeberg.org/mhughes9
- https://codeberg.org/micash
- https://codeberg.org/michael-sparrow
- https://codeberg.org/Miguel_PL
- https://codeberg.org/mirkoperillo
- https://codeberg.org/mlncn
- https://codeberg.org/mortzu
- https://codeberg.org/MSalah73
- https://codeberg.org/msrd0
- https://codeberg.org/mstuehrenberg
- https://codeberg.org/Mynacol
- https://codeberg.org/n0toose
- https://codeberg.org/n2o
- https://codeberg.org/nasmevka
- https://codeberg.org/natct
- https://codeberg.org/natkr
- https://codeberg.org/Neureka
- https://codeberg.org/neuschaefer
- https://codeberg.org/Neustradamus
- https://codeberg.org/NiclasPe
- https://codeberg.org/nobodyinperson
- https://codeberg.org/Nordfriese
- https://codeberg.org/oliverpool
- https://codeberg.org/omenos
- https://codeberg.org/Omichron
- https://codeberg.org/OrbitalMartian
- https://codeberg.org/Outbreak2096
- https://codeberg.org/panekj
- https://codeberg.org/pasabanov
- https://codeberg.org/pat-s
- https://codeberg.org/patka
- https://codeberg.org/pietervdvn
- https://codeberg.org/pipcet
- https://codeberg.org/PlayBoxTech
- https://codeberg.org/post-factum
- https://codeberg.org/programmerjake
- https://codeberg.org/Protestant
- https://codeberg.org/PurpleBooth
- https://codeberg.org/puzzle-it-nu
- https://codeberg.org/r-w-x
- https://codeberg.org/rabit
- https://codeberg.org/rdwz
- https://codeberg.org/revi
- https://codeberg.org/Ricky-Tigg
- https://codeberg.org/Routhinator
- https://codeberg.org/rudolph
- https://codeberg.org/Ryuno-Ki
- https://codeberg.org/s-light
- https://codeberg.org/s1m
- https://codeberg.org/salif
- https://codeberg.org/Salt
- https://codeberg.org/salzig
- https://codeberg.org/sammefishe
- https://codeberg.org/sclu1034
- https://codeberg.org/scMarkus
- https://codeberg.org/sdomi
- https://codeberg.org/sg-phoenix-technologies
- https://codeberg.org/shgew
- https://codeberg.org/Skivling
- https://codeberg.org/Slate3919
- https://codeberg.org/smxi
- https://codeberg.org/snematoda
- https://codeberg.org/Snoweuph
- https://codeberg.org/SomeTr
- https://codeberg.org/Spinning9247
- https://codeberg.org/stb
- https://codeberg.org/stop5
- https://codeberg.org/tacaly
- https://codeberg.org/ted423
- https://codeberg.org/thefinn93
- https://codeberg.org/theoryshaw
- https://codeberg.org/thilinajayanath
- https://codeberg.org/thodorisl
- https://codeberg.org/ThomasBoom89
- https://codeberg.org/thor
- https://codeberg.org/Thyxus
- https://codeberg.org/tkbremnes
- https://codeberg.org/tusooa
- https://codeberg.org/tuxmaster
- https://codeberg.org/ujr
- https://codeberg.org/umglurf
- https://codeberg.org/unikitty
- https://codeberg.org/viceice
- https://codeberg.org/virtulis
- https://codeberg.org/voltagex
- https://codeberg.org/vpeltnt
- https://codeberg.org/vpotyarkin
- https://codeberg.org/waldyrious
- https://codeberg.org/wetneb
- https://codeberg.org/wneessen
- https://codeberg.org/wolftune
- https://codeberg.org/Wuzzy
- https://codeberg.org/wvffle
- https://codeberg.org/xavidcr
- https://codeberg.org/XeroX
- https://codeberg.org/xmort
- https://codeberg.org/xtex
- https://codeberg.org/yaash
- https://codeberg.org/yann-soubeyrand
- https://codeberg.org/yannicka
- https://codeberg.org/ytimenkov
- https://codeberg.org/zukka77
