---
title: Forgejo monthly update - March 2024
publishDate: 2024-03-31
tags: ['news', 'report']
excerpt: Forgejo 7.0.0 release candidates are now available for testing at https://v7.next.forgejo.org or by downloading OCI images and binaries, updated daily. It will be the first LTS release, supported until July 2025. Four new translations that were added are Filipino, Esperanto, Slovenian and Bulgarian and the localization team keeps growing.
---

The monthly report is meant to provide a good overview of what has changed in Forgejo in the past month. If you would like to help, please get in touch in [the chatroom](https://matrix.to/#/#forgejo-chat:matrix.org) or participate in the [ongoing discussions](https://codeberg.org/forgejo/discussions).

Forgejo 7.0.0 release candidates are now available for testing at https://v7.next.forgejo.org or by downloading OCI images ([root](https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/7.0-test) / [rootless](https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/7.0-test-rootless)) and [binaries](https://codeberg.org/forgejo-experimental/forgejo/releases/tag/v7.0-test) that are updated updated daily.

The `7.0.0+LTS-gitea-1.22.0` version is the first Long Term Support (LTS) release and will receive critical bug and security fixes until July 2025. It is an additional burden on Forgejo contributors and members of the release team. Individuals and organizations who need that kind of stability are kindly invited to contribute to this effort so it can be sustained in the long run.

New languages that got active translators and have reached a usable level of completion [were added](https://codeberg.org/forgejo/forgejo/pulls/2724) and are now visible to users: Bulgarian, Esperanto, Filipino and Slovenian.

## New release management

Now that [Forgejo forked its own way forward](https://forgejo.org/2024-02-forking-forward/), it became necessary to define when and how releases are published.

### Semantic versioning

The first Forgejo version to be published with its own release management will be `7.0.0+LTS-gitea-1.22.0` and the [documentation was updated](https://forgejo.org/docs/v7.0/user/versions/) to explain the new numbering scheme.

### Gitea API compatibility

Tools that are developed for the Gitea API will keep working with the new Forgejo numbering scheme. They typically make assertions on the release number to unlock new functionalities and that logic will not be impacted by a bump in the release number. The proprietary version of Gitea has a different numbering scheme (v21.X.Y, v22.X.Y) and is in a similar situation.

### Forgejo 7.0 release candidates

The [7.0/forgejo](https://codeberg.org/forgejo/forgejo/src/branch/v7.0/forgejo) branch was cut 30 March 2024 and the [v8.0.0-dev](https://codeberg.org/forgejo/forgejo/releases/tag/v8.0.0-dev) tag set to the development branch. While the release-critical bugs are being fixed so that the version can be published, https://v7.next.forgejo.org will keep being updated daily with a build of the latest commit, acting as a release candidate where anyone can safely try and break it. It can also be installed locally with:

- OCI images: [root](https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/7.0-test) and [rootless](https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/7.0-test-rootless)
- [Binaries](https://codeberg.org/forgejo-experimental/forgejo/releases/tag/v7.0-test)

Their name stays the same but they are replaced by a new build every day.

### Forgejo 7.0 LTS

Now that Forgejo has given itself the mean to avoid most of the regressions that it suffered from in the past, it can provide support for releases during a longer period of time. The `7.0.0+LTS-gitea-1.22.0` version is the first Long Term Support (LTS) release and will receive critical bug and security fixes until July 2025.

### Time based release schedule

The [time based release schedule](https://forgejo.org/docs/v7.0/developer/release/#release-cycle) was established to publish a release every three months. Patch releases will be published more frequently, depending on the severity of the bug or security fixes they contain. The exact number of the release cannot be known in advance because it will be determined by the features and breaking changes it contains, as specified by the [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html) specifications.

| **Date** | **Version**            | **Release date** | **End Of Life**  |
| -------- | ---------------------- | ---------------- | ---------------- |
| 2024 Q1  | 7.0.0+LTS-gitea-1.22.0 | 17 April 2024    | **16 July 2025** |
| 2024 Q2  | X.Y.Z+gitea-A.B.C      | 17 July 2024     | 16 October 2024  |
| 2024 Q3  | X.Y.Z+gitea-A.B.C      | 16 October 2024  | 15 January 2025  |
| 2024 Q4  | X.Y.Z+gitea-A.B.C      | 15 January 2025  | 16 April 2025    |

## Code

https://codeberg.org/forgejo/forgejo

Notable improvements and bug fixes:

- [Webhook refactor and addition of SourceHut builds](https://codeberg.org/forgejo/forgejo/pulls/2717) is a series of 7 pull requests, 6 of which have been merged.
- A number of localization improvements ([1](https://codeberg.org/forgejo/forgejo/pulls/2828), [2](https://codeberg.org/forgejo/forgejo/pulls/2756), [3](https://codeberg.org/forgejo/forgejo/pulls/2644), [4](https://codeberg.org/forgejo/forgejo/pulls/2612), [5](https://codeberg.org/forgejo/forgejo/pulls/2610), [6](https://codeberg.org/forgejo/forgejo/pulls/2584), [7](https://codeberg.org/forgejo/forgejo/pulls/2492), etc.)
- Tests [now fail instead of just displaying error logs](https://codeberg.org/forgejo/forgejo/pulls/2657). This should help catch missing translations and ensure that an error is logged only when an inconsistency is detected within Forgejo (which shouldn’t happen if the test is realistically setup).
- [Recognize SSH signed tags in addition to OpenPGP](https://codeberg.org/forgejo/forgejo/pulls/2520).
- [Add S3 bucket lookup type](https://codeberg.org/forgejo/forgejo/pulls/2482) and [documentation](https://codeberg.org/forgejo/docs/pulls/465).

[Read more](https://codeberg.org/forgejo/forgejo/pulls?q=&type=all&sort=&state=closed&labels=&milestone=0&project=0&assignee=0&poster=0) in the pull requests.

## Backport automation

When pull requests are merged, a [workflow will automatically open a backport](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/.forgejo/workflows/backport.yml). If the `backport/v1.21` label is found, it will [target the Forgejo v1.21 branch](https://codeberg.org/forgejo/forgejo/pulls/2827). If there are no conflicts and tests pass, it can be merged right away and save valuable time.

As Forgejo 7.0 enters the release candidate stage of its life cycle a significant number of backports are expected during the first few weeks and such an automation will have even more of an impact.

## Dependency Management

A [dependency update detection](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/.forgejo/workflows/renovate.yml) cron job has been setup to automatically [open pull request proposing updates](https://codeberg.org/forgejo/forgejo/pulls?state=closed&poster=165503) when new versions of Go or JavaScript packages, OCI images and more are available. It [finds the release notes](https://codeberg.org/forgejo/forgejo/pulls/2800), adds them to the pull request description and Forgejo contributors can then conveniently decide whether an upgrade is necessary. This recurring observations of Forgejo dependencies is [fine tuned by a configuration file](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/renovate.json) that, among other things, ensures there are at most five pull requests proposed at all times.

The [weekly cherry-pick of commits from Gitea](https://codeberg.org/forgejo/forgejo/pulls?q=gitea%20week&state=closed) became a routine collectively managed by two Forgejo contributors. It is one of the improvements brought by the hard fork decision from last month. Rebasing was more involved and managed by a single person: an undesirable single point of failure. A [tool](https://codeberg.org/forgejo/tools) was created to automate the most tedious and error prone tasks.

## End to end tests

The [end-to-end tests](https://code.forgejo.org/forgejo/end-to-end) were entirely refactored for unification and simplicity. They cover the following areas:

- [actions](https://code.forgejo.org/forgejo/end-to-end/src/branch/main/actions)
- [packages](https://code.forgejo.org/forgejo/end-to-end/src/branch/main/packages)
- [upgrades](https://code.forgejo.org/forgejo/end-to-end/src/branch/main/upgrade)
- [storage](https://code.forgejo.org/forgejo/end-to-end/src/branch/main/storage)

They can now all be run by adding the `run-end-to-end-test` label to a pull request (see [this example](https://codeberg.org/forgejo/forgejo/pulls/2465)). Before the refactor, only Forgejo Actions tests were run.

## Localization

New languages that got active translators and have reached a usable level of completion [were added](https://codeberg.org/forgejo/forgejo/pulls/2724) and are now visible to users: Bulgarian, Esperanto, Filipino and Slovenian.

New contributors were onboarded ([1](https://codeberg.org/forgejo/governance/pulls/98), [2](https://codeberg.org/forgejo/governance/pulls/108)) to the localization team.

The English source strings saw a lot of activity for unification and rewording. There were no notable conflicts with Gitea translations. There was one conflict caused by merged pull request in Forgejo that locked translations for about half of a day. Although Weblate owns all translated files and they are not to be modified in pull requests, there are exceptions. When mass modification can be automated, this can be done by a pull request and to save valuable translators' time. But when it happens, care must be taken to avoid conflicts and [the required steps were documented](https://forgejo.org/docs/v1.21/developer/localization-admin/#merging-a-pull-request-that-changes-translations).

The upside of this incident was to show that resolving such conflicts is as simple as reverting the faulty commit and opening a new pull request. It is not the most efficient way to go about it, but it is simple.

## Federation

The pull request to implement [federated stars](https://codeberg.org/forgejo/forgejo/pulls/1680) made progress. In the settings of a repository there is now a field allowing to define its federated repositories. Read [more in the activity summary](https://domaindrivenarchitecture.org/posts/2024-03-27-state-of-federation/).

The [federation implementation task list](https://codeberg.org/forgejo/forgejo/issues/59) was updated.

## Helm chart

The Forgejo helm chart had [one major updates](https://codeberg.org/forgejo-contrib/forgejo-helm/releases) because of a major bump of the postgresql dependencies.

References:

- https://codeberg.org/forgejo-contrib/forgejo-helm/releases

## Runner

The [Forgejo runner version 3.4.1](https://code.forgejo.org/forgejo/runner/src/branch/main/RELEASE-NOTES.md#3-4-0) was published and supports for the artifacts@v4 protocol when used with the development version of Forgejo 7.0, as [demonstrated by the end-to-end tests](https://code.forgejo.org/forgejo/end-to-end/src/branch/main/actions/example-artifacts-v4/.forgejo/workflows/test.yml).

With the caveat that a forked version of the matching [download](https://code.forgejo.org/forgejo/download-artifact) and [upload](https://code.forgejo.org/forgejo/upload-artifact) actions must be used because they both assume a GitHub environment. Chasing such features is high maintenance and casts a doubt on its long term viability. The Forgejo runner makes no promise of compatibility with GitHub and an alternative action with the same interface but implemented differently may be, for example, a more sustainable choice.

References

- https://code.forgejo.org/forgejo/runner
- https://code.forgejo.org/forgejo/act

## Governance

### Sustainability

A [discussion started](https://codeberg.org/forgejo/discussions/issues/144) in an attempt to find an answer and make Forgejo durable for the next 10 years.

A [grant proposal](https://codeberg.org/forgejo/sustainability/pulls/41) was drafted to support the development of Forgejo.

An additional [Request for Payment was sent](https://codeberg.org/forgejo/sustainability/pulls/42) to NLnet in the context of the [ongoing grant](https://codeberg.org/forgejo/sustainability/pulls?labels=123038) and the funds will go to Codeberg.

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/0ko
- https://codeberg.org/6543
- https://codeberg.org/abacabadabacaba
- https://codeberg.org/acioustick
- https://codeberg.org/airbreather
- https://codeberg.org/algernon
- https://codeberg.org/Andre601
- https://codeberg.org/antoyo
- https://codeberg.org/aral
- https://codeberg.org/axd99
- https://codeberg.org/banaanihillo
- https://codeberg.org/be4zad
- https://codeberg.org/captainepoch
- https://codeberg.org/cherryb
- https://codeberg.org/con-f-use
- https://codeberg.org/crystal
- https://codeberg.org/dachary
- https://codeberg.org/dasasd122311
- https://codeberg.org/dboerlage
- https://codeberg.org/defanor
- https://codeberg.org/denyskon
- https://codeberg.org/Dirk
- https://codeberg.org/doomedguppy
- https://codeberg.org/Drakon
- https://codeberg.org/earl-warren
- https://codeberg.org/Edgarsons
- https://codeberg.org/ell1e
- https://codeberg.org/eo
- https://codeberg.org/EOWNERDEAD
- https://codeberg.org/EssGeeEich
- https://codeberg.org/Eveeifyeve
- https://codeberg.org/ezra
- https://codeberg.org/f00
- https://codeberg.org/fauxmight
- https://codeberg.org/Fjuro
- https://codeberg.org/flactwin
- https://codeberg.org/flipreverse
- https://codeberg.org/fnetX
- https://codeberg.org/foxy
- https://codeberg.org/GamePlayer-8
- https://codeberg.org/Gusted
- https://codeberg.org/HarryK
- https://codeberg.org/Haui
- https://codeberg.org/hazy
- https://codeberg.org/hexa
- https://codeberg.org/inference
- https://codeberg.org/intelfx
- https://codeberg.org/jadedctrl
- https://codeberg.org/jadeprime
- https://codeberg.org/JakobDev
- https://codeberg.org/james2432
- https://codeberg.org/jean-daricade
- https://codeberg.org/JeremyStarTM
- https://codeberg.org/jerger
- https://codeberg.org/jilen
- https://codeberg.org/jmshrtn
- https://codeberg.org/KaKi87
- https://codeberg.org/kallisti5
- https://codeberg.org/kita
- https://codeberg.org/lampajr
- https://codeberg.org/Laxystem
- https://codeberg.org/ledyba
- https://codeberg.org/lyssieth
- https://codeberg.org/Mai-Lapyst
- https://codeberg.org/MatseVH
- https://codeberg.org/maunzCache
- https://codeberg.org/maytha8
- https://codeberg.org/mishra
- https://codeberg.org/mmarif
- https://codeberg.org/mondstern
- https://codeberg.org/MorsMortium
- https://codeberg.org/msrd0
- https://codeberg.org/mumulhl
- https://codeberg.org/n0toose
- https://codeberg.org/neox_
- https://codeberg.org/nis
- https://codeberg.org/nmmr
- https://codeberg.org/noth
- https://codeberg.org/OdinVex
- https://codeberg.org/oliverpool
- https://codeberg.org/ormai
- https://codeberg.org/payas
- https://codeberg.org/Pi-Cla
- https://codeberg.org/popey
- https://codeberg.org/pyfisch
- https://codeberg.org/RaptaG
- https://codeberg.org/realaravinth
- https://codeberg.org/rikh
- https://codeberg.org/salif
- https://codeberg.org/sertonix
- https://codeberg.org/SinTan1729
- https://codeberg.org/skobkin
- https://codeberg.org/snematoda
- https://codeberg.org/SteffoSpieler
- https://codeberg.org/tampler
- https://codeberg.org/Techwizz
- https://codeberg.org/tek256
- https://codeberg.org/tengkuizdihar
- https://codeberg.org/TheAwiteb
- https://codeberg.org/thefinn93
- https://codeberg.org/thefox
- https://codeberg.org/thepaperpilot
- https://codeberg.org/timmwille
- https://codeberg.org/tuxcoder
- https://codeberg.org/twenty-panda
- https://codeberg.org/uncomfyhalomacro
- https://codeberg.org/VehementHam
- https://codeberg.org/viceice
- https://codeberg.org/wangito33
- https://codeberg.org/wetneb
- https://codeberg.org/WithLithum
- https://codeberg.org/wolfogre
- https://codeberg.org/Wuzzy
- https://codeberg.org/Xinayder
- https://codeberg.org/yeziruo
- https://codeberg.org/zareck
- https://codeberg.org/zbolo-wd
- https://codeberg.org/zenobit
- https://codeberg.org/zotan
- https://codeberg.org/Zughy

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
