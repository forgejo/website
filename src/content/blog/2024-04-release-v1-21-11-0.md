---
title: Forgejo Security Release 1.21.11-0
publishDate: 2024-04-18
tags: ['releases', 'security']
release: 1.21.11-0
excerpt: "The Forgejo v1.21.11-0 release contains two security fixes: a privilege escalation that allows any registered user to change the visibility of any public repository; and a cross-site scripting (XSS) vulnerability that enabled attackers to run unsandboxed client-side scripts on pages served from the forge's domain."
---

[Forgejo v1.21.11-0](https://codeberg.org/forgejo/forgejo/releases/tag/v1.21.11-0) was released 18 April 2024.

This release contains _two security fixes_, both of which can be exploited by registered Forgejo users. One flaw allows _anyone_ who can open a pull request against a repository to change its visibility. The other lets the attacker run unsandboxed client-side scripts on pages served from the forge's domain, a [Cross-site scripting (XSS)](https://en.wikipedia.org/wiki/Cross-site_scripting) vulnerability.

### Recommended Action

We _strongly recommend_ that all Forgejo installations are upgraded to the latest version as soon as possible.

### Privilege escalation through `git push` options

By far the more serious issue is a privilege escalation through `git push` options, which allowed any registered user to change the visibility of any repository they could see - public repositories included -, regardless of what level of access they had.

A more detailed deep-dive about this vulnerability will follow in a later blog post.

### Cross-site scripting (XSS) vulnerability

The other vulnerability is a [Cross-site scripting (XSS)](https://en.wikipedia.org/wiki/Cross-site_scripting) vulnerability that could be exploited by a registered Forgejo user.

In certain situations, rendered repository contents weren't properly guarded, and allowed unsandboxed client-side scripts to run from the same domain as the forge itself.

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo, we'd love to hear from you! Open an issue on [our issue tracker](https://codeberg.org/forgejo/forgejo/issues) for feature requests or bug reports. You can also find us [on the Fediverse](https://floss.social/@forgejo), or drop by [our Matrix space](https://matrix.to/#/#forgejo:matrix.org) ([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) to say hi!
