---
title: Gitea 1.22 is the last version to allow a transparent upgrade to Forgejo
publishDate: 2024-12-18
tags: ['news']
excerpt: The Forgejo v10.0.0 release to be published 15 January 2025 supports upgrade from Gitea instances up to version v1.22 included. Future Forgejo versions will not support upgrades from Gitea instances running version v1.23 or above.
---

The Forgejo v10.0.0 release to be published 15 January 2025 supports
upgrade from Gitea instances up to version v1.22 included. Future
Forgejo versions will not support upgrades from Gitea instances
running version v1.23 or above.

## How will this impact me?

If you are running a Gitea instance and would like to upgrade your instance, here are your options:

- Gitea versions up to v1.21 can be upgraded to Forgejo v7.0 (Long Term Support) or higher, up to v10.0 included
- Gitea v1.22 can be upgraded to Forgejo v8.0 or higher, up to v10.0 included

Upgrade from Gitea to Forgejo versions higher than v10.0 is a two step process:

- Migrate Gitea to Forgejo (e.g. Gitea v1.21 to Forgejo v7.0.10)
- Upgrade Forgejo to the desired version (e.g. Forgejo v7.0.10 to Forgejo v11.0.5)

If you are running Gitea 1.23 or higher, it may be possible to upgrade
to Forgejo v8.0.0 or higher, if you are ready to manually modify the
database and move the data around. You can get help in attempting that
but you should not expect more than a best effort.

If you would like to move data from a Gitea instance that cannot be
upgraded to Forgejo, you can move repositories one-by-one using the
in-app migration tool. It will continue to work for as long as the
Gitea API remains stable.

## Why is this happening?

In 2023 Forgejo was a soft fork, a set of patches maintained by the Forgejo
community on top of Gitea. In 2024 [it became a hard
fork](/2024-02-forking-forward/) and the codebases started to
diverge.

A very significant effort was made by Forgejo contributors to keep
both codebases compatible and share the bug fixes and features.

- [Tooling was developed](https://codeberg.org/forgejo/tools) to watch
  over the Gitea codebase.
- [Weekly cherry-pick sessions](https://codeberg.org/forgejo/forgejo/pulls?q=[gitea]+week&type=all&fuzzy=false)
  were organized to discuss Gitea commits and evaluate their relevance in
  Forgejo.

In the beginning `git cherry-pick` was possible without conflict most
of the time. But it is less often the case because large scale
features were implemented in Forgejo such as
[quotas](https://forgejo.org/docs/v9.0/admin/quota/) and Gitea engaged
in dozens of refactors.

Forgejo and Gitea are now effectively different codebases although
they share the same history back from the early days of Gogs. Features
and bug fixes are a source of inspiration to be ported manually and no
longer material for a clean `cherry-pick`.

## Why now?

Most hard fork do not invest so much time and effort to keep in sync
with the original codebase. And for a reason: it turns out to not be
very exciting. But it was very beneficial while it lasted: dozens of
bug fixes landed in Forgejo and allowed contributors to focus on more
interesting matters.

A year ago it was impossible to guess how long it would take for
Forgejo to become incompatible with Gitea. It was inevitable but it
could have happened a lot sooner.

Forgejo is incompatible with Gitea v1.23 for instance because:

- Modifications in the Gitea database are not in the Forgejo database
  (e.g. indices in actions, priority for protected branches)
- Some Gitea features have a completely different implementation in
  Forgejo (e.g. admin branch protection)
- Some sub systems have been re-architectured deeply in Forgejo or Gitea
  (e.g. webhooks in Forgejo)

Such differences were worked on as part of the [weekly
chore](https://codeberg.org/forgejo/forgejo/pulls?q=[gitea]+week&type=all&fuzzy=false)
to keep the two codebases close to each other and ensure a upgrade
from Gitea to Forgejo was a seamless experience. As can be expected,
the difficulty increases in direct proportion of the drift between the
two codebases.

It reached a point where the benefits are no longer worth the effort.

## Would Gitea be willing to help?

If Gitea was a community driven project, this may have been
possible. But the reason for Forgejo to exist was [disagreement about
the governance of the project](https://gitea-open-letter.coding.social/),
and naturally this situation has not improved. By now,
[Gitea is Open Core](https://codeberg.org/forgejo/discussions/issues/102)
and several attempts by the Forgejo community to upstream changes have lead to
tensions.

In the past year Gitea did not make any effort to keep both codebases
compatible or help Forgejo. It actually did the exact opposite a
number of times, re-implementing bug fixes or features shortly after
they were published in Forgejo. In doing so, Gitea has intentionally
or carelessly precipitated the divergence.
