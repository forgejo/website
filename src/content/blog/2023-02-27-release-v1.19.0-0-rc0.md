---
title: Forgejo v1.19 release candidates
publishDate: 2023-02-27
tags: ['releases', 'rc']
release: 1.19.0-0-rc0
excerpt: The first Forgejo v1.19 release candidate is ready for testing. Checkout the release notes for a preview of the new features.
---

Today [Forgejo
v1.19.0-0-rc0](https://codeberg.org/forgejo-experimental/forgejo/releases/tag/v1.19.0-0-rc0)
was released. It is meant for testing only: do not upgrade a
production instance with it.

The [draft release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#draft-1-19-0-0) contain a summary of the new features. The highlights are:

- [Incoming emails](https://codeberg.org/forgejo/forgejo/commit/fc037b4b825f0501a1489e10d7c822435d825cb7)
- [Scoped labels](https://codeberg.org/forgejo/forgejo/commit/6221a6fd5)
- Package registries now support for [Cargo](https://codeberg.org/forgejo/forgejo/commit/df789d962), [Conda](https://codeberg.org/forgejo/forgejo/commit/6ba9ff7b4) and [Chef](https://codeberg.org/forgejo/forgejo/commit/d987ac6bf). They also have new [cleanup rules](https://codeberg.org/forgejo/forgejo/commit/32db62515) and [quota limits](https://codeberg.org/forgejo/forgejo/commit/20674dd05).
- Any webhook can now [specify an `Authorization` header](https://codeberg.org/forgejo/forgejo/commit/b6e81357bd6fb80f8ba94c513f89a210beb05313) to be sent along every request.

The [Actions experimental
CI/CD](/2023-02-27-forgejo-actions), although not
ready for real world usage, is also present and [can be used to run a
demo](/2023-02-27-forgejo-actions).

Make sure to [check the breaking
changes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#draft-1-19-0-0)
and get your production instance ready for when the v1.19 release is
available.

### Try it out

The release candidate is published in [the dedicated "experimental"
Forgejo organization](https://codeberg.org/forgejo-experimental) and
can be downloaded from:

- Containers at https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/1.19
- Binaries at https://codeberg.org/forgejo-experimental/forgejo/releases/tag/v1.19.0-0-rc0

They will be updated based on your feedback until they become robust enough to be released.

### Help write good release notes

The best release notes are meant to articulate the needs and benefits
of new features and the actions recommended for breaking changes so
Forgejo admins quickly know if it is of interest to them.

The [current draft release
notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#draft-1-19-0-0)
contain a complete inventory but the descriptions are still
incomplete. They will be finished by the time the release is published
and you can help make them better.

Please submit your own descriptions and comments in [our issue tracker](https://codeberg.org/forgejo/forgejo/issues/new/choose?milestone=3489).

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo, we'd love to hear from you!
Open an issue on [our issue tracker](https://codeberg.org/forgejo/forgejo/issues)
for feature requests or bug reports. You can also find us [on the Fediverse](https://floss.social/@forgejo),
or drop by [our Matrix space](https://matrix.to/#/#forgejo:matrix.org)
([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) to say hi!
