---
title: Forgejo monthly update - December 2024
publishDate: 2025-01-05
tags: ['news', 'report']
excerpt: All the best for the new year from Forgejo. We are in the preparations for the upcoming v10 release, have improved testing and automation. A a big thanks to everyone who makes Forgejo a success, including upstream projects and libraries Forgejo depends on.
---

The monthly report is meant to provide a good overview of what has changed in Forgejo in the past month. If you would like to help, please get in touch in [the chatroom](https://matrix.to/#/#forgejo-chat:matrix.org) or participate in the [ongoing discussions](https://codeberg.org/forgejo/discussions).

We wish all contributors, Forgejo users and interested followers all the best for 2025, especially for the projects you develop on Forgejo.

## Welcome to Forgejo

It makes us very happy to hear from so many people who are migrating from various services to Forgejo, often based on their new year's resolutions. We are looking forward to see you as active members in our community and appreciate your feedback as new users.

We are pleased to read that the Fedora project is [going to use Forgejo](https://communityblog.fedoraproject.org/fedora-chooses-forgejo/), and we are excited to improve the software and project in mutual collaboration. Some Forgejo contributors have met [Fedora community members at FOSDEM 2024](https://codeberg.org/forgejo/discussions/issues/115) and we are looking forward to a nice long-term partnership.

## Visual testing

Decent test coverage to guard against bugs and regressions is an important goal of the Forgejo maintainers. However, testing the user-visible frontend is often challenging. In recent months, a lot of browser tests have been written using [Playwright](https://playwright.dev/) (also see the recent monthly updates) to test the behaviour of Forgejo as seen by a user.

Now, the tests are additionally used to create and compare screenshots to enable [visual regression testing](https://codeberg.org/forgejo/discussions/issues/257). The screenshots are managed in [a separate repository](https://code.forgejo.org/forgejo/visual-browser-testing/) to keep the size of the Forgejo repository low.

The screenshots are compared against previous state to see if pixel's match exactly. If they don't, a contributor reviews if the difference is expected (e.g. due to an improvement), acceptable (e.g. a minor side effect due to a change) or poses an issue. A few [minor issues](https://codeberg.org/forgejo/forgejo/issues/6385) and [regressions](https://codeberg.org/forgejo/forgejo/pulls/6344#issuecomment-2549594) have already been discovered and fixed with the help of the screenshots.

This workflow is more efficient than manually clicking through Forgejo's UI, because test coverage is guaranteed and measurable. It is more accurate than human vision alone. The only remaining task is verifying the few changed pages.

## Development work

There are only two weeks to go before the v10 Forgejo release (scheduled for January 15, see the [release schedule](https://forgejo.org/docs/next/admin/release-schedule/)). If you want to help make the release a great success, please [finish translations](https://forgejo.org/docs/latest/contributor/localization/) and optionally test the release early and report issues.

Numerous improvements have made it into the release in December, such as [searching in a subpath of your repo](https://codeberg.org/forgejo/forgejo/pulls/6143), performance improvements for [notifications](https://codeberg.org/forgejo/forgejo/pulls/6146) and [new repo dialog](https://codeberg.org/forgejo/forgejo/pulls/6100), [OpenGraph previews for repositories and releases](https://codeberg.org/forgejo/forgejo/pulls/6269) and more.

Several forms have been reworked to make use of the [new forms styleguide](https://codeberg.org/forgejo/design/src/branch/main/guides/forms.md). They have improved consistency and accessibility and user experience was slightly improved. You will be available to enjoy a [simplified new repo](https://codeberg.org/forgejo/forgejo/pulls/6386) dialog and [adjusted profile settings](https://codeberg.org/forgejo/forgejo/pulls/6407) in the next release, [and more to come](https://codeberg.org/forgejo/forgejo/pulls/6361).

Please report any accessibility issues and minor inconvenience to either the issue tracker or Mastodon.

## Releases

The Forgejo release process is documented as a checklist of actions to be performed by [a member of the release team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#release) (see for instance [the checklist for the 9.0.2 patch release](https://codeberg.org/forgejo/forgejo/issues/5972) or [the checklist for the 9.0.0 major release](https://codeberg.org/forgejo/forgejo/issues/5380)).

Work started [last month](https://forgejo.org/2024-11-monthly-update/#forgejo-releases) to automate most of the tasks. In a nutshell, the new [release manager](https://code.forgejo.org/forgejo/release-manager) creates a [list of tasks](https://code.forgejo.org/forgejo/release-tasks) for a given release (it is different for a major release or a patch release). Each task is an issue in a dedicated Forgejo repository that was created for this purpose only.

The description of the issue (such as cutting a branch) explains what is going to be done automatically and what is the responsibility of the release team member (such as writing a blog post). By placing the `run` label on the issue, the release manager will carry out the task. In `dry run` mode it shows what would be done and documents it as a comment on the issue. The gory details are found in the output of the workflow, for debugging purposes.

The [Forgejo v9.0.3](https://codeberg.org/forgejo/forgejo/milestone/8833) release was published using the release manager for the first time. It was a bumpy experience that uncovered a dozen bugs and even brought down code.forgejo.org during 15 minutes. Although the individual actions involved in the process are tested (see [this example test workflow](https://code.forgejo.org/forgejo/release-scheduler/src/commit/0f105cac9225eae823b9d5cf63da2335039e3cd1/.forgejo/workflows/test.yml)), bugs happen.

The Forgejo helm chart has seen [two security updates](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases).

## Infrastructure

The migration of the [Forgejo infrastructure to a k8s cluster](https://forgejo.org/2024-11-monthly-update/#infrastructure) that was completed last month can be used to improve the availability of [the default instance](https://forgejo.org/docs/v9.0/admin/actions/#default-actions-url) from which actions are downloaded.

As of Forgejo v10.0 [actions will be downloaded from https://data.forgejo.org](https://codeberg.org/forgejo/forgejo/pulls/6313), separating read-only assets that can easily be replicated and updated with a delay from the repositories where development happen such as [end-to-end tests](https://code.forgejo.org/forgejo/end-to-end) or the [Forgejo runner](https://code.forgejo.org/forgejo/runner).

## Dependency management

Almost a year ago, a large scale effort was undertaken to [improve dependency management](https://forgejo.org/docs/next/contributor/dependencies/) on the [Forgejo repository itself](https://codeberg.org/forgejo/forgejo/src/commit/ec20eaee44375ee065379e698feb1cedd9891923/renovate.json).

It was generalized to all Forgejo instances and repositories involved in the making of Forgejo. To keep it dry, [a shared configuration](https://code.forgejo.org/forgejo/renovate-config) is used. A number of repositories where dependencies were lagging behind were updated ([the setup-forgejo action](https://code.forgejo.org/actions/setup-forgejo/pulls/248) is one of many such updates).

## Sustainability

Two ongoing grants have ended this month, the first one on December 1st and the second on December 31rd. We are thankful for [the NLnet foundation](https://NLnet.nl/) for the great funding opportunity.

The funds compensated the work of Forgejo contributors with a total € 40.250.
Generously, some contributors have decided to send their request for payment in the name of Codeberg e.V., which has received a total of € 32.750 from the NLnet grants, multiplying the effect of the grant by allowing the money to be re-invested to sustain the development of Forgejo.

Unfortunately, some portion of the grant was not claimed and expired, mostly because contributors were not available to complete the work they initially planned to do.

During a [sustainability meeting on December 28](https://codeberg.org/forgejo/sustainability/src/branch/main/meetings/2024-12-28.md), ideas and plans for next funding opportunities were exchanged. The goal is to complete a list of potential milestones within two weeks and propose a workplan to NLnet.

If financial compensation would allow you to contribute to Forgejo, we are interested in hearing from you. You can join the chat of the sustainability team on Matrix: [#forgejo-sustainability:matrix.org](https://matrix.to/#/%23forgejo-sustainability%3Amatrix.org).

## Acknowledgements to projects that improve Forgejo

Forgejo exists thanks to thousands of contributors in the free software ecosystem. They maintain libraries and development tools that save the Forgejo contributors time and effort. Their impact on the codebase is significant, but it is often only visible in case bugs originate from these upstream developers.

In the future, we would like to add [explicit acknowledgements](https://codeberg.org/forgejo/discussions/issues/252) to the contributors of the ecosystem that makes Forgejo what it is, and we appreciate help and pointers on how to efficiently do this.

There is no secrecy about our relation to Gitea, the project we forked two years ago, and we have applied due diligence and best practices to retain authorship information and references for code that we imported from the Gitea open source codebase.

Nevertheless, we have been [accused of stealing code from Gitea](https://codeberg.org/forgejo/discussions/issues/251#issuecomment-2513035), combined with a threat for legal action by a Gitea contributor and CommitGO/Gitea Ltd. shareholder. Apparently, the primary issue was confusion about the cherry-picks, commit signatures as well as the exact definition of "hard fork". Note that we rely on the default behaviour of Git when used with GPG keys. Since not everyone involved in the thread can be expected to be familiar with the characteristics of Git, we [explained the behaviour](https://codeberg.org/forgejo/discussions/issues/251#issuecomment-2513108).

In short: `git cherry-pick` retains the authorship of the commit as the "author" metadata, and signs the commit to preserve information about who created the cherry-pick. Signing the commits with GPG keys of Gitea contributors is obviously not possible, because only they have access to their private keys and sharing them would undermine the security benefits of the signatures.

To clear up any confusion, we have reworded certain parts of our website and explain the current cherry-picking process once more.
Nearly every week, a Forgejo contributor [uses a tool](https://codeberg.org/forgejo/tools/src/branch/main/scripts) to categorize activity in Gitea and create a [pull request](https://codeberg.org/forgejo/forgejo/pulls/6391) that contains references to the origin of contributions and an assessment of relevancy for Forgejo.

A small subset of contributions is picked because they are relevant to Forgejo and save effort for the Forgejo contributors. Some more commits are marked for a closer look, for instance because test coverage is not sufficient for a merge in Forgejo, because they require more discussion and review for usability, accessibility and design, or because there is a conflict that needs resolution (we refer to this as "porting"). Many commits are skipped for various reasons, e.g. because they have already been implemented in Forgejo before, because they are specific to Gitea's workflows, or otherwise are not agreed on by Forgejo reviewers. For an impression, the stats for the last weekly cherry-pick look like this:

> Between gitea@35c86af164 and gitea@a92f5057ae, 41 commits have been reviewed. We picked 4, skipped 31 (of which 6 were already in Forgejo!), and decided to port 6.

After the set of cherry-picks has been reviewed and approved, the commits are merged so that they retain authorship information about the Gitea contributors.

Over time, the two codebases will diverge further, and this procedure will become unfeasible some day. While contributors are still free to port individual changes from Gitea, it will stop being a routine task.

We hope this reduces confusion about our current and expected future workflow. Please let us know in case there are any questions left or suggestions from your side.

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/0ko
- https://codeberg.org/71rd
- https://codeberg.org/achyrva
- https://codeberg.org/AdamMajer
- https://codeberg.org/Alex619829
- https://codeberg.org/algernon
- https://codeberg.org/amano.kenji
- https://codeberg.org/anbraten
- https://codeberg.org/Andre601
- https://codeberg.org/angelnu
- https://codeberg.org/antaanimosity
- https://codeberg.org/aqtrans
- https://codeberg.org/artnay
- https://codeberg.org/ashimokawa
- https://codeberg.org/atalanttore
- https://codeberg.org/avobs
- https://codeberg.org/awiteb
- https://codeberg.org/becm
- https://codeberg.org/Beowulf
- https://codeberg.org/blumia
- https://codeberg.org/bmorel
- https://codeberg.org/BSD
- https://codeberg.org/c8h4
- https://codeberg.org/cdotnow
- https://codeberg.org/CEbbinghaus
- https://codeberg.org/cobak78
- https://codeberg.org/codenyte
- https://codeberg.org/ConfusedOnCOFFEE
- https://codeberg.org/Crown0815
- https://codeberg.org/crystal
- https://codeberg.org/Cwpute
- https://codeberg.org/d2735
- https://codeberg.org/d96b
- https://codeberg.org/danielbaumann
- https://codeberg.org/David-Guillot
- https://codeberg.org/ddriggs
- https://codeberg.org/Dirk
- https://codeberg.org/dmowitz
- https://codeberg.org/doasu
- https://codeberg.org/dragon
- https://codeberg.org/earl-warren
- https://codeberg.org/EchedelleLR
- https://codeberg.org/Edgarsons
- https://codeberg.org/ell1e
- https://codeberg.org/emansije
- https://codeberg.org/Ember
- https://codeberg.org/emilylange
- https://codeberg.org/eNBeWe
- https://codeberg.org/end3r-man
- https://codeberg.org/ExtremelyMAD
- https://codeberg.org/faoquad
- https://codeberg.org/Fjuro
- https://codeberg.org/fnetX
- https://codeberg.org/Frankkkkk
- https://codeberg.org/furkangkhsn
- https://codeberg.org/gabrielgio
- https://codeberg.org/george.bartolomey
- https://codeberg.org/gratux
- https://codeberg.org/Gravy59
- https://codeberg.org/Gusted
- https://codeberg.org/hacknorris
- https://codeberg.org/itsTurnip
- https://codeberg.org/jak2k
- https://codeberg.org/JakobDev
- https://codeberg.org/jalil
- https://codeberg.org/jerger
- https://codeberg.org/jinn
- https://codeberg.org/jkirk
- https://codeberg.org/jmakov
- https://codeberg.org/JohnMoon-VTS
- https://codeberg.org/jornfranke
- https://codeberg.org/JSchlarb
- https://codeberg.org/julianfoad
- https://codeberg.org/jutty
- https://codeberg.org/jwildeboer
- https://codeberg.org/KaKi87
- https://codeberg.org/kampka
- https://codeberg.org/kidsan
- https://codeberg.org/kita
- https://codeberg.org/klausfyhn
- https://codeberg.org/Kokomo
- https://codeberg.org/lapo
- https://codeberg.org/Laxystem
- https://codeberg.org/LinuxinaBit
- https://codeberg.org/litchipi
- https://codeberg.org/lslalbai
- https://codeberg.org/lunny
- https://codeberg.org/Lzebulon
- https://codeberg.org/m_eiman_tlab
- https://codeberg.org/mahlzahn
- https://codeberg.org/Maks1mS
- https://codeberg.org/malik-n
- https://codeberg.org/Maniues
- https://codeberg.org/marcellmars
- https://codeberg.org/martinwguy
- https://codeberg.org/Matze99
- https://codeberg.org/mbateman
- https://codeberg.org/meaz
- https://codeberg.org/Merith-TK
- https://codeberg.org/metamuffin
- https://codeberg.org/mfenniak
- https://codeberg.org/MHLut
- https://codeberg.org/MichaelAgarkov
- https://codeberg.org/mimot
- https://codeberg.org/mirkoperillo
- https://codeberg.org/mlncn
- https://codeberg.org/MrEvil
- https://codeberg.org/mvdkleijn
- https://codeberg.org/n0toose
- https://codeberg.org/nazunalika
- https://codeberg.org/nette
- https://codeberg.org/ng-dm
- https://codeberg.org/Nirei
- https://codeberg.org/nobodyinperson
- https://codeberg.org/Nordfriese
- https://codeberg.org/nostar
- https://codeberg.org/notpushkin
- https://codeberg.org/oliverpool
- https://codeberg.org/Outbreak2096
- https://codeberg.org/pasabanov
- https://codeberg.org/pedroberg
- https://codeberg.org/pgmtx
- https://codeberg.org/post-factum
- https://codeberg.org/programmerjake
- https://codeberg.org/pylixonly
- https://codeberg.org/QazCetelic
- https://codeberg.org/R1ckSanchez_C137
- https://codeberg.org/r3pek
- https://codeberg.org/rosna2321
- https://codeberg.org/scabala
- https://codeberg.org/scruel
- https://codeberg.org/sdomi
- https://codeberg.org/senorsmile
- https://codeberg.org/sg-phoenix-technologies
- https://codeberg.org/ShalokShalom
- https://codeberg.org/shaowenwan
- https://codeberg.org/SLASHLogin
- https://codeberg.org/snematoda
- https://codeberg.org/Snoweuph
- https://codeberg.org/solonovamax
- https://codeberg.org/SomeTr
- https://codeberg.org/spiffyk
- https://codeberg.org/stevenroose
- https://codeberg.org/tacaly
- https://codeberg.org/therealpim
- https://codeberg.org/thezzisu
- https://codeberg.org/thilinajayanath
- https://codeberg.org/timotheyca
- https://codeberg.org/tobru
- https://codeberg.org/tusooa
- https://codeberg.org/tuxmaster
- https://codeberg.org/VadZ
- https://codeberg.org/viceice
- https://codeberg.org/voltagex
- https://codeberg.org/vpotyarkin
- https://codeberg.org/wang3076
- https://codeberg.org/wetneb
- https://codeberg.org/wisher
- https://codeberg.org/withlithum
- https://codeberg.org/wneessen
- https://codeberg.org/wolftune
- https://codeberg.org/Wuzzy
- https://codeberg.org/xorander00
- https://codeberg.org/xtex
- https://codeberg.org/xtrm
- https://codeberg.org/yeziruo
- https://codeberg.org/yp05327
- https://codeberg.org/zukka77
